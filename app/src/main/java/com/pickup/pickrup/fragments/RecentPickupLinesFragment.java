package com.pickup.pickrup.fragments;// Created by Sanat Dutta on 2/17/2015.

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.pickup.pickrup.R;
import com.pickup.pickrup.activities.CommentActivity;
import com.pickup.pickrup.activities.LoginActivity;
import com.pickup.pickrup.activities.MainScreenActivity;
import com.pickup.pickrup.activities.UserProfileViewActivity;
import com.pickup.pickrup.common.ShareVia;
import com.pickup.pickrup.common.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RecentPickupLinesFragment extends Fragment {

    private String TAG = "RecentPickupLinesFragment";

    //Interfaces
    private pickupLineCardAdapter mAdapter;

    //Views
    private ListView mListView;
    private ProgressBarCircularIndeterminate fetchingDataProgressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    //Data
    private boolean isDataFetching = false, isEverythingLoaded = false;
    private int PICKUP_LINES_PER_PAGE = 20;
    private Date lastLoadedDate = null;
    private ArrayList<ParseObject> pickupLinesObjects = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recent, container, false);

        Log.i(TAG, "RecentPickupLinesFragment: onCreateView");

        findViews(view);
        mAdapter = new pickupLineCardAdapter();
        mListView.setAdapter(mAdapter);
        setUpSwipeRefreshLayout();
        getPickUpLines(null);

        return view;
    }

    private void getPickUpLines(final Date mDate) {
        isDataFetching = true;

        Log.i(TAG, "Fetching PickUpLines");
        ParseQuery<ParseObject> pickupLinesObjectsQuery = new ParseQuery("PickUpLines");
        pickupLinesObjectsQuery.whereNotEqualTo("visible", false);
        pickupLinesObjectsQuery.setLimit(PICKUP_LINES_PER_PAGE);
        pickupLinesObjectsQuery.addDescendingOrder("createdAt");
        if (mDate != null) pickupLinesObjectsQuery.whereLessThan("createdAt", mDate);
        pickupLinesObjectsQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> pickUpLinelist, ParseException e) {
                fetchingDataProgressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                if (e == null) {
                    if (pickUpLinelist.size() > 0) {
                        Log.i(TAG, "PickUpLines Fetching Finished");
                        mSwipeRefreshLayout.setBackgroundColor(getResources().getColor(R.color.facebook_background));
                        if (mDate == null) pickupLinesObjects.clear();
                        pickupLinesObjects.addAll(pickUpLinelist);
                        lastLoadedDate = pickupLinesObjects.get(pickupLinesObjects.size() - 1).getCreatedAt();
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (mDate == null) {
                            Log.e(TAG, "No Recent PickUpLines");
                            pickupLinesObjects.clear();
                            mAdapter.notifyDataSetChanged();
                            mSwipeRefreshLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                        } else {
                            isEverythingLoaded = true;
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    Log.e(TAG, "Unable to fetch PickupLines. Error: " + e);
                    Toast.makeText(getActivity(), "Unable to fetch PickupLines", Toast.LENGTH_SHORT).show();
                }
                isDataFetching = false;
            }
        });
    }

    private void setUpSwipeRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(TAG, "onRefresh");
                if (!isDataFetching) {
                    isEverythingLoaded = false;
                    lastLoadedDate = null;
                    getPickUpLines(lastLoadedDate);
                } else Log.i(TAG, "Data Fetch Ongoing");
            }
        });
    }

    private void findViews(View view) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.pickup_lines_swipe_refresh_layout);
        mListView = (ListView) view.findViewById(R.id.pickup_lines_listview);
        fetchingDataProgressBar = (ProgressBarCircularIndeterminate) view.findViewById(R.id.fetchingDataProgressBar);
    }

    private class pickupLineCardAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;

        private pickupLineCardAdapter() {
            mLayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return pickupLinesObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return pickupLinesObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View mView;
            if ((position == (pickupLinesObjects.size() - 1)) && (pickupLinesObjects.size() > PICKUP_LINES_PER_PAGE - 1) && !isEverythingLoaded)
                mView = mLayoutInflater.inflate(R.layout.item_pickup_line_last, parent, false);
            else mView = mLayoutInflater.inflate(R.layout.item_pickup_line, parent, false);
            final ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.pickUpLineGenderIcon = (ImageView) mView.findViewById(R.id.pickup_item_gender_icon);
            mViewHolder.pickUpLineUserName = (TextView) mView.findViewById(R.id.pickup_item_username);
            mViewHolder.pickUpLineTime = (TextView) mView.findViewById(R.id.pickup_item_time);
            mViewHolder.pickUpLineShare = (ImageView) mView.findViewById(R.id.pickup_item_share);
            mViewHolder.tagGeneral = (TextView) mView.findViewById(R.id.tag_general);
            mViewHolder.tagCS = (TextView) mView.findViewById(R.id.tag_cs);
            mViewHolder.tagDirty = (TextView) mView.findViewById(R.id.tag_dirty);
            mViewHolder.pickUpLineTextView = (TextView) mView.findViewById(R.id.pickup_line_text_view);
            mViewHolder.pickUpLineBookmarkIcon = (ImageView) mView.findViewById(R.id.pickup_item_bookmark);
            mViewHolder.pickUpLineLikeClick = (LinearLayout) mView.findViewById(R.id.pickup_item_like_click);
            mViewHolder.pickUpLineDislikeClick = (LinearLayout) mView.findViewById(R.id.pickup_item_dislike_click);
            mViewHolder.pickUpLineCommentIcon = (ImageView) mView.findViewById(R.id.pickup_item_comment_icon);
            mViewHolder.pickUpLineLikeIcon = (ImageView) mView.findViewById(R.id.pickup_item_like_icon);
            mViewHolder.pickUpLineDislikeIcon = (ImageView) mView.findViewById(R.id.pickup_item_dislike_icon);
            mViewHolder.pickUpLineLikeText = (TextView) mView.findViewById(R.id.pickup_item_like_text);
            mViewHolder.pickUpLineDislikeText = (TextView) mView.findViewById(R.id.pickup_item_dislike_text);
            mViewHolder.pickUpLineCommentText = (TextView) mView.findViewById(R.id.pickup_item_comment_text);

            if (pickupLinesObjects.get(position).getString("submittedUserGender").equals("Female"))
                mViewHolder.pickUpLineGenderIcon.setImageResource(R.drawable.ic_gender_female);
            mViewHolder.pickUpLineUserName.setText(pickupLinesObjects.get(position).getString("submittedUserName"));
            mViewHolder.pickUpLineTime.setText(Utils.getTimeDifference(pickupLinesObjects.get(position).getCreatedAt()));
            if (pickupLinesObjects.get(position).getBoolean("TAG_GENERAL")) mViewHolder.tagGeneral.setVisibility(View.VISIBLE);
            if (pickupLinesObjects.get(position).getBoolean("TAG_CS")) mViewHolder.tagCS.setVisibility(View.VISIBLE);
            if (pickupLinesObjects.get(position).getBoolean("TAG_DIRTY")) mViewHolder.tagDirty.setVisibility(View.VISIBLE);
            mViewHolder.pickUpLineTextView.setText(pickupLinesObjects.get(position).getString("pickUpLine"));
            if (MainScreenActivity.pickUpLinesBookmarksArray != null) {
                if (MainScreenActivity.pickUpLinesBookmarksArray.contains(pickupLinesObjects.get(position).getObjectId()))
                    mViewHolder.pickUpLineBookmarkIcon.setImageResource(R.drawable.ic_bookmark_blue_18dp);
            }
            if (MainScreenActivity.pickUpLinesLikesArray != null) {
                if (MainScreenActivity.pickUpLinesLikesArray.contains(pickupLinesObjects.get(position).getObjectId()))
                    mViewHolder.pickUpLineLikeIcon.setImageResource(R.drawable.ic_like_active);
            }
            if (MainScreenActivity.pickUpLinesDislikesArray != null) {
                if (MainScreenActivity.pickUpLinesDislikesArray.contains(pickupLinesObjects.get(position).getObjectId()))
                    mViewHolder.pickUpLineDislikeIcon.setImageResource(R.drawable.ic_dislike_active);
            }
            mViewHolder.pickUpLineLikeText.setText(Utils.validateLikeDislikeCount(pickupLinesObjects.get(position).getInt("likes")));
            mViewHolder.pickUpLineDislikeText.setText(Utils.validateLikeDislikeCount(pickupLinesObjects.get(position).getInt("dislikes")));
            mViewHolder.pickUpLineCommentText.setText(""+pickupLinesObjects.get(position).getInt("commentCount"));

            mViewHolder.pickUpLineTextView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Utils.copyText(getActivity(), pickupLinesObjects.get(position).getString("pickUpLine"));
                    return true;
                }
            });
            View.OnClickListener mOnclickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.pickup_item_username:
                            goToProfileViewActivity();
                            break;
                        case R.id.pickup_item_share:
                            ShareVia.findAvailableSharePackages(getActivity(), pickupLinesObjects.get(position).getString("pickUpLine"));
                            break;
                        case R.id.pickup_line_text_view:
                            goToCommentActivity();
                            break;
                        case R.id.pickup_item_bookmark:
                            if (ParseUser.getCurrentUser() != null) {
                                bookmarkPickUpLine();
                            } else goToLoginActivity();
                            break;
                        case R.id.pickup_item_like_click:
                            if (ParseUser.getCurrentUser() != null) {
                                likeClickPickUpLine();
                            } else
                                goToLoginActivity();
                            break;
                        case R.id.pickup_item_dislike_click:
                            if (ParseUser.getCurrentUser() != null) {
                                dislikeClickPickUpLine();
                            } else
                                goToLoginActivity();
                            break;
                        case R.id.pickup_item_comment_icon:
                            goToCommentActivity();
                            break;
                    }
                }

                private void goToCommentActivity() {
                    Intent goToCommentActivityIntent = new Intent(getActivity(), CommentActivity.class);
                    goToCommentActivityIntent.putExtra("submittedUserId", pickupLinesObjects.get(position).getString("submittedUserId"));
                    goToCommentActivityIntent.putExtra("submittedUserName", pickupLinesObjects.get(position).getString("submittedUserName"));
                    goToCommentActivityIntent.putExtra("objectId", pickupLinesObjects.get(position).getObjectId());
                    goToCommentActivityIntent.putExtra("pickUpLine", pickupLinesObjects.get(position).getString("pickUpLine"));
                    goToCommentActivityIntent.putExtra("likes", pickupLinesObjects.get(position).getInt("likes"));
                    goToCommentActivityIntent.putExtra("dislikes", pickupLinesObjects.get(position).getInt("dislikes"));
                    startActivity(goToCommentActivityIntent);
                }

                private void goToProfileViewActivity() {
                    Intent goToProfileViewActivityIntent = new Intent(getActivity(), UserProfileViewActivity.class);
                    goToProfileViewActivityIntent.putExtra("userObjectId", pickupLinesObjects.get(position).getString("submittedUserId"));
                    goToProfileViewActivityIntent.putExtra("userName", pickupLinesObjects.get(position).getString("submittedUserName"));
                    startActivity(goToProfileViewActivityIntent);
                }

                private void dislikeClickPickUpLine() {
                    // if dislike array is null, create a new one
                    if (MainScreenActivity.pickUpLinesDislikesArray == null)
                        MainScreenActivity.pickUpLinesDislikesArray = new ArrayList<>();

                    // if dislike array does not contain the object
                    if (!MainScreenActivity.pickUpLinesDislikesArray.contains(pickupLinesObjects.get(position).getObjectId())) {
                        MainScreenActivity.pickUpLinesDislikesArray.add(pickupLinesObjects.get(position).getObjectId());

                        // increase dislike count locally
                        pickupLinesObjects.get(position).put("dislikes", pickupLinesObjects.get(position).getInt("dislikes") + 1);

                        //Increment the dislike count on the pickupline object
                        ParseObject dislikeIncrement = ParseObject.createWithoutData("PickUpLines", pickupLinesObjects.get(position).getObjectId());
                        dislikeIncrement.increment("dislikes");
                        dislikeIncrement.saveInBackground();

                        if (MainScreenActivity.pickUpLinesLikesArray != null) {
                            if (MainScreenActivity.pickUpLinesLikesArray.contains(pickupLinesObjects.get(position).getObjectId())) {
                                MainScreenActivity.pickUpLinesLikesArray.remove(pickupLinesObjects.get(position).getObjectId());

                                if (pickupLinesObjects.get(position).getInt("likes") > 0) {

                                    // decrease like count locally
                                    pickupLinesObjects.get(position).put("likes", pickupLinesObjects.get(position).getInt("likes") - 1);

                                    //Decrement the like count on the pickupline object
                                    ParseObject likeDecrement = ParseObject.createWithoutData("PickUpLines", pickupLinesObjects.get(position).getObjectId());
                                    likeDecrement.increment("likes", -1);
                                    likeDecrement.saveInBackground();
                                }
                            }
                        }

                        // Update the User Disike Array with pickupline object id
                        ParseUser.getCurrentUser().put("pickUpLinesDislikes", MainScreenActivity.pickUpLinesDislikesArray);
                        ParseUser.getCurrentUser().saveInBackground();
                        notifyDataSetChanged();
                    }
                }

                private void likeClickPickUpLine() {
                    // if like array is null, create a new one
                    if (MainScreenActivity.pickUpLinesLikesArray == null)
                        MainScreenActivity.pickUpLinesLikesArray = new ArrayList<>();

                    // if like array does not contain the object
                    if (!MainScreenActivity.pickUpLinesLikesArray.contains(pickupLinesObjects.get(position).getObjectId())) {
                        MainScreenActivity.pickUpLinesLikesArray.add(pickupLinesObjects.get(position).getObjectId());

                        // increase like count locally
                        pickupLinesObjects.get(position).put("likes", pickupLinesObjects.get(position).getInt("likes") + 1);

                        //Increment the like count on the pickupline object
                        ParseObject likeIncrement = ParseObject.createWithoutData("PickUpLines", pickupLinesObjects.get(position).getObjectId());
                        likeIncrement.increment("likes");
                        likeIncrement.saveInBackground();

                        if (MainScreenActivity.pickUpLinesDislikesArray != null) {
                            if (MainScreenActivity.pickUpLinesDislikesArray.contains(pickupLinesObjects.get(position).getObjectId())) {
                                MainScreenActivity.pickUpLinesDislikesArray.remove(pickupLinesObjects.get(position).getObjectId());

                                if (pickupLinesObjects.get(position).getInt("dislikes") > 0) {

                                    // decrease dislike count locally
                                    pickupLinesObjects.get(position).put("dislikes", pickupLinesObjects.get(position).getInt("dislikes") - 1);

                                    //Decrement the dislike count on the pickupline object
                                    ParseObject dislikeDecrement = ParseObject.createWithoutData("PickUpLines", pickupLinesObjects.get(position).getObjectId());
                                    dislikeDecrement.increment("dislikes", -1);
                                    dislikeDecrement.saveInBackground();
                                }
                            }
                        }

                        // Update the User Like Array with pickupline object id
                        ParseUser.getCurrentUser().put("pickUpLinesLikes", MainScreenActivity.pickUpLinesLikesArray);
                        ParseUser.getCurrentUser().saveInBackground();
                        notifyDataSetChanged();
                    }
                }

                private void bookmarkPickUpLine() {
                    if (MainScreenActivity.pickUpLinesBookmarksArray == null)
                        MainScreenActivity.pickUpLinesBookmarksArray = new ArrayList<>();

                    if (MainScreenActivity.pickUpLinesBookmarksArray.contains(pickupLinesObjects.get(position).getObjectId()))
                        MainScreenActivity.pickUpLinesBookmarksArray.remove(pickupLinesObjects.get(position).getObjectId());
                    else
                        MainScreenActivity.pickUpLinesBookmarksArray.add(pickupLinesObjects.get(position).getObjectId());

                    ParseUser.getCurrentUser().put("pickUpLinesBookmarks", MainScreenActivity.pickUpLinesBookmarksArray);
                    ParseUser.getCurrentUser().saveInBackground();
                    notifyDataSetChanged();
                }

                private void showPickupCardMorePopUp(View view) {
                    PopupMenu mPopupMenu = new PopupMenu(getActivity(), view);
                    mPopupMenu.getMenuInflater().inflate(R.menu.menu_more_popup, mPopupMenu.getMenu());

                    mPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_report_user:
                                    break;
                            }
                            return true;
                        }
                    });
                    mPopupMenu.show();
                }
            };

            mViewHolder.pickUpLineUserName.setOnClickListener(mOnclickListener);
            mViewHolder.pickUpLineTextView.setOnClickListener(mOnclickListener);
            mViewHolder.pickUpLineShare.setOnClickListener(mOnclickListener);
            mViewHolder.pickUpLineBookmarkIcon.setOnClickListener(mOnclickListener);
            mViewHolder.pickUpLineLikeClick.setOnClickListener(mOnclickListener);
            mViewHolder.pickUpLineDislikeClick.setOnClickListener(mOnclickListener);
            mViewHolder.pickUpLineCommentIcon.setOnClickListener(mOnclickListener);

            // Fetch more data
            if ((position == (pickupLinesObjects.size() - 1)) && !isDataFetching && !isEverythingLoaded)
                getPickUpLines(lastLoadedDate);

            return mView;
        }

        private class ViewHolder {
            private ImageView pickUpLineGenderIcon;
            private TextView pickUpLineUserName;
            private TextView pickUpLineTime;
            private ImageView pickUpLineShare;
            private TextView tagGeneral, tagCS, tagDirty;
            private TextView pickUpLineTextView;
            private ImageView pickUpLineBookmarkIcon;
            private LinearLayout pickUpLineLikeClick;
            private LinearLayout pickUpLineDislikeClick;
            private ImageView pickUpLineCommentIcon;
            private ImageView pickUpLineLikeIcon;
            private ImageView pickUpLineDislikeIcon;
            private TextView pickUpLineLikeText;
            private TextView pickUpLineDislikeText;
            private TextView pickUpLineCommentText;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && pickupLinesObjects.size() > 0) {
            Log.i(TAG, TAG + " is visible");
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, TAG + " onResume()");
        mAdapter.notifyDataSetChanged();
    }

    private void goToLoginActivity() {
        Intent goToLoginActivityIntent = new Intent(getActivity(), LoginActivity.class);
        startActivity(goToLoginActivityIntent);
        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
