package com.pickup.pickrup.common;// Created by Sanat Dutta on 2/18/2015.

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SendCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class Utils {

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting());
    }

    public static void hideKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mInputMethodManager = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mInputMethodManager.hideSoftInputFromWindow(windowToken, 0);
    }

    public boolean doesPackageExist(Context context, String targetPackage) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public static String getTimeDifference(Date createdAt) {
        Date currentDate = new Date();
        long dateDifference = Math.abs(currentDate.getTime() - createdAt.getTime());

        long years = dateDifference / 31104000000L;
        if (years > 0) {
            return years + "Y";
        }
        long months = dateDifference / 2592000000L;
        if (months > 0) {
            return months + "M";
        }
        long days = dateDifference / 86400000;
        if (days > 0) {
            return days + "D";
        }
        long hours = dateDifference / 3600000;
        if (hours > 0) {
            return hours + "h";
        }
        long mins = dateDifference / 60000;
        if (mins > 0) {
            return mins + "m";
        }
        long secs = dateDifference / 1000;
        if (secs > 0) {
            return secs + "s";
        }
        return "0s";
    }

    public static String validateLikeDislikeCount(int count) {
        if (count > 999) {
            if (count % 1000 < 100) return ("" + (count / 1000) + "k");
            else {
                float roundedCount = count / (float) 1000;
                return ("" + String.format("%.1f", roundedCount) + "k");
            }
        } else return ("" + count);
    }

    public static void copyText(Context context, String pickUpLine) {
        android.content.ClipboardManager mClipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData mClipData = android.content.ClipData.newPlainText("Copied Text", pickUpLine);
        mClipboard.setPrimaryClip(mClipData);

        Toast.makeText(context, "Pickup Line Copied", Toast.LENGTH_SHORT).show();
    }

    public static void increasePickUpLineCommentCount(String objectId) {
        ParseObject commentCountIncrement = ParseObject.createWithoutData("PickUpLines", objectId);
        commentCountIncrement.increment("commentCount", 1);
        commentCountIncrement.saveInBackground();
    }

    public static void sendCommentNotification(final String submittedUserId, final String submittedUserName, final String commentUserId, final String commentUserName, final String objectId, final String pickUpLine, final int likes, final int dislikes, final String commentText) {
        Log.i("Utils", "Sending Push Notification");

        ParseObject pickUpLineObject = ParseObject.createWithoutData("PickUpLines", objectId);
        pickUpLineObject.fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e==null){
                    ArrayList<String> commentUsers = new ArrayList<>();
                    JSONArray commentJsonArray = parseObject.getJSONArray("commentUsers");
                    if (commentJsonArray != null) {
                        Log.i("Utils", "CommentUsers Not Null");
                        for (int i=0;i<commentJsonArray.length();i++){
                            try {
                                commentUsers.add(commentJsonArray.get(i).toString());
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                                Log.e("Utils", "JSON Error");
                            }
                        }
                    }

                    commentUsers.add(submittedUserId);
                    if (commentUsers.contains(commentUserId))commentUsers.remove(commentUserId);
                    sendNotification(commentUsers);
                }
                else Log.e("Utils", "Cannot fetch pickUpLine. Error: "+e.getMessage());
            }

            private void sendNotification(ArrayList<String> commentUsers) {
                ParseQuery<ParseInstallation> pushQuery = ParseInstallation.getQuery();
                pushQuery.whereEqualTo("channels", "commentNotification");
                pushQuery.whereContainedIn("deviceId", commentUsers);


                JSONObject data = null;
                try {
                    data = new JSONObject("{"
                            + "\"action\": \"com.pickup.pickrup.VIEW_PICKUP_LINE\","
                            + "\"commentText\": \"" + commentText + "\","
                            + "\"submittedUserId\": \"" + submittedUserId + "\","
                            + "\"submittedUserName\": \"" + submittedUserName + "\","
                            + "\"commentUserId\": \"" + commentUserId + "\","
                            + "\"commentUserName\": \"" + commentUserName + "\","
                            + "\"objectId\": \"" + objectId + "\","
                            + "\"pickUpLine\": \"" + pickUpLine + "\","
                            + "\"likes\": \"" + likes + "\","
                            + "\"dislikes\": \"" + dislikes + "\"}");

                    //Send Push notification
                    ParsePush push = new ParsePush();
                    push.setQuery(pushQuery);
                    push.setData(data);
                    push.sendInBackground(new SendCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e!=null) Log.i("Utils", "Error: "+e.getMessage()+" "+e.getCode());
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Utils", "Sending Push Notification: Error");
                }
            }
        });
    }

    public static Bitmap decodeImageResource(Resources res, int resId, double reqWidth, double reqHeight) {
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        int bitmapWidth = options.outWidth;
        int bitmapHeight = options.outHeight;
        int scaleFactor = Math.max((int) Math.round(bitmapWidth/reqWidth), (int) Math.round(bitmapHeight/reqHeight));
        options.inSampleSize = scaleFactor;
        options.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeResource(res, resId, options);
        return bitmap;
    }

    public static void registerUserToPickupLineCommentList(String pickUpLineObjectId, final String commentUserObjectId) {

        Log.i("Utils", "Registering User To PickupLine CommentList");

        ParseObject pickUpLineObject = ParseObject.createWithoutData("PickUpLines", pickUpLineObjectId);
        pickUpLineObject.fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e==null){
                    ArrayList<String> commentUsers = new ArrayList<>();
                    JSONArray commentJsonArray = parseObject.getJSONArray("commentUsers");
                    if (commentJsonArray != null) {
                        Log.i("Utils", "CommentUsers Not Null");
                        for (int i=0;i<commentJsonArray.length();i++){
                            try {
                                commentUsers.add(commentJsonArray.get(i).toString());
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                                Log.e("Utils", "JSON Error");
                            }
                        }
                        if (!commentUsers.contains(commentUserObjectId)) commentUsers.add(commentUserObjectId);
                    }
                    else{
                        Log.i("Utils", "CommentUsers Null");
                        commentUsers.add(commentUserObjectId);
                    }

                    parseObject.put("commentUsers", commentUsers);
                    parseObject.saveInBackground();
                } else Log.e("Utils", "Cannot fetch pickUpLine. Error: "+e.getMessage());
            }
        });
    }
}
