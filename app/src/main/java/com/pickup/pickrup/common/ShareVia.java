package com.pickup.pickrup.common;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pickup.pickrup.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanat Dutta on 3/9/2015.
 */
public class ShareVia {

    public static void findAvailableSharePackages(Context context, String pickUpLine) {
        String TAG = "ShareVia";
        PackageManager mPackageManager = context.getPackageManager();
        List<ApplicationInfo> mPackages = mPackageManager.getInstalledApplications(PackageManager.GET_META_DATA);
        List<String> packageNames = new ArrayList<>();

        for (ApplicationInfo mPackage : mPackages) {
            packageNames.add(mPackage.packageName);
        }

        String[] packageNameArray = context.getResources().getStringArray(R.array.packageNameArray);

        ArrayList<Boolean> packageBoolean = new ArrayList<>();
        for (int i = 0; i < packageNameArray.length; i++) {
            if (packageNames.contains(packageNameArray[i]))
                packageBoolean.add(i, true);
            else packageBoolean.add(i, false);
        }

        Log.i(TAG, "" + packageBoolean);

        showShareDialog(context, pickUpLine,packageBoolean, packageNameArray);
    }

    private static void showShareDialog(final Context context, final String pickUpLine, ArrayList<Boolean> packageBoolean, final String[] packageNameArray) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Send To")
                .titleColorRes(R.color.teal_700)
                .customView(R.layout.dialog_share, true)
                .build();

        RelativeLayout showAllTextView = (RelativeLayout) dialog.getCustomView().findViewById(R.id.item_share_showall);
        TextView itemShareWhatsApp = (TextView) dialog.getCustomView().findViewById(R.id.item_share_whatsapp);
        TextView itemShareLine = (TextView) dialog.getCustomView().findViewById(R.id.item_share_line);
        TextView itemShareKik = (TextView) dialog.getCustomView().findViewById(R.id.item_share_kik);
        TextView itemShareWeChat = (TextView) dialog.getCustomView().findViewById(R.id.item_share_wechat);
        TextView itemShareNineChat = (TextView) dialog.getCustomView().findViewById(R.id.item_share_ninechat);
        TextView itemShareFaceBook = (TextView) dialog.getCustomView().findViewById(R.id.item_share_facebook);
        TextView itemShareMessenger = (TextView) dialog.getCustomView().findViewById(R.id.item_share_messenger);
        TextView itemShareHangout = (TextView) dialog.getCustomView().findViewById(R.id.item_share_hangout);
        TextView itemShareHiChat = (TextView) dialog.getCustomView().findViewById(R.id.item_share_hichat);
        TextView itemShareTwitter = (TextView) dialog.getCustomView().findViewById(R.id.item_share_twitter);

        ArrayList<TextView> mItemShares = new ArrayList<>();
        mItemShares.add(itemShareWhatsApp);
        mItemShares.add(itemShareLine);
        mItemShares.add(itemShareKik);
        mItemShares.add(itemShareWeChat);
        mItemShares.add(itemShareNineChat);
        mItemShares.add(itemShareFaceBook);
        mItemShares.add(itemShareMessenger);
        mItemShares.add(itemShareHangout);
        mItemShares.add(itemShareHiChat);
        mItemShares.add(itemShareTwitter);

        for (int i=0; i<packageBoolean.size(); i++){
            if (!packageBoolean.get(i)) mItemShares.get(i).setVisibility(View.GONE);
        }

        View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.item_share_showall:
                        dialog.dismiss();
                        showNativeShareChooser();
                        break;
                    case R.id.item_share_whatsapp:
                        dialog.dismiss();
                        shareViaApp(0);
                        break;
                    case R.id.item_share_line:
                        dialog.dismiss();
                        shareViaApp(1);
                        break;
                    case R.id.item_share_kik:
                        dialog.dismiss();
                        shareViaApp(2);
                        break;
                    case R.id.item_share_wechat:
                        dialog.dismiss();
                        shareViaApp(3);
                        break;
                    case R.id.item_share_ninechat:
                        dialog.dismiss();
                        shareViaApp(4);
                        break;
                    case R.id.item_share_facebook:
                        dialog.dismiss();
                        shareViaApp(5);
                        break;
                    case R.id.item_share_messenger:
                        dialog.dismiss();
                        shareViaApp(6);
                        break;
                    case R.id.item_share_hangout:
                        dialog.dismiss();
                        shareViaApp(7);
                        break;
                    case R.id.item_share_hichat:
                        dialog.dismiss();
                        shareViaApp(8);
                        break;
                    case R.id.item_share_twitter:
                        dialog.dismiss();
                        shareViaApp(9);
                        break;
                }
            }

            private void showNativeShareChooser() {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, pickUpLine);
                context.startActivity(Intent.createChooser(shareIntent, "Share via"));
            }

            private void shareViaApp(int appId) {
                Intent mIntent = new Intent(android.content.Intent.ACTION_SEND);
                mIntent.setType("text/plain");
                mIntent.putExtra(android.content.Intent.EXTRA_TEXT, pickUpLine);
                mIntent.setPackage(packageNameArray[appId]);
                context.startActivity(Intent.createChooser(mIntent, "Share via"));
            }
        };
        showAllTextView.setOnClickListener(mOnClickListener);
        itemShareWhatsApp.setOnClickListener(mOnClickListener);
        itemShareLine.setOnClickListener(mOnClickListener);
        itemShareKik.setOnClickListener(mOnClickListener);
        itemShareWeChat.setOnClickListener(mOnClickListener);
        itemShareNineChat.setOnClickListener(mOnClickListener);
        itemShareFaceBook.setOnClickListener(mOnClickListener);
        itemShareMessenger.setOnClickListener(mOnClickListener);
        itemShareHangout.setOnClickListener(mOnClickListener);
        itemShareHiChat.setOnClickListener(mOnClickListener);
        itemShareTwitter.setOnClickListener(mOnClickListener);

        dialog.show();
    }
}
