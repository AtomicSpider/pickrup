package com.pickup.pickrup.common;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.pickup.pickrup.R;
import com.pickup.pickrup.activities.CommentActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sanat Dutta on 4/15/2015.
 */
public class ParsePushNotificationReceiver extends BroadcastReceiver {

    private String TAG = "ParsePushNotificationReceiver";

    //Data
    private String action, submittedUserId, submittedUserName, commentUserId, commentUserName, objectId, pickUpLine, commentText, message, uri;
    private int likes, dislikes;

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));

            if (json.has("action")) action = json.getString("action");

            if (action.equals("com.pickup.pickrup.VIEW_PICKUP_LINE")) {

                if (json.has("submittedUserId"))
                    submittedUserId = json.getString("submittedUserId");

                if (json.has("submittedUserName"))
                    submittedUserName = json.getString("submittedUserName");

                if (json.has("commentUserId"))
                    commentUserId = json.getString("commentUserId");

                if (json.has("commentUserName"))
                    commentUserName = json.getString("commentUserName");

                if (json.has("objectId"))
                    objectId = json.getString("objectId");

                if (json.has("pickUpLine"))
                    pickUpLine = json.getString("pickUpLine");

                if (json.has("commentText"))
                    commentText = json.getString("commentText");

                if (json.has("likes"))
                    likes = json.getInt("likes");

                if (json.has("dislikes"))
                    dislikes = json.getInt("dislikes");

                generateCustomNotificationComment(context, submittedUserId, submittedUserName, commentUserId, commentUserName, objectId, pickUpLine, commentText, likes, dislikes);
            }
            if (action.equals("com.pickup.pickrup.PARSE_URI")){
                if (json.has("message"))
                    message = json.getString("message");

                if (json.has("uri"))
                    uri = json.getString("uri");

                generateCustomNotificationUri(context, message, uri);
            }
        } catch (JSONException e) {
            Log.d(TAG , "JSONException: " + e.getMessage());
        }
    }

    private void generateCustomNotificationUri(Context context, String message, String uri) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder.setSmallIcon(R.drawable.ic_notification)
                .setTicker(message)
                .setLargeIcon(((BitmapDrawable)context.getResources().getDrawable(R.drawable.ic_launcher)).getBitmap())
                .setContentTitle("PickrUp")
                .setContentText(message);
        //.setNumber(notifications)
        //.setStyle(new NotificationCompat.BigTextStyle().bigText(""));

        Intent notificationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));

        int NOTIFICATION_ID = uri.hashCode();
        PendingIntent intent = PendingIntent.getActivity(context, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(intent);
        Notification notification = builder.build();
        //notification.bigContentView = bigView;
        notification.defaults = Notification.DEFAULT_ALL;
        notification.ledARGB = Color.GREEN;
        notification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    private void generateCustomNotificationComment(Context context, String submittedUserId, String submittedUserName, String commentUserId, String commentUserName, String objectId, String pickUpLine, String commentText, int likes, int dislikes) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder.setSmallIcon(R.drawable.ic_notification)
                .setTicker(commentUserName+" commented on a post")
                .setLargeIcon(((BitmapDrawable)context.getResources().getDrawable(R.drawable.ic_launcher)).getBitmap())
                .setContentTitle("PickrUp")
                .setContentText(commentUserName+" commented: "+commentText)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(commentUserName+" commented: "+commentText));
                //.setNumber(notifications)
                //.setStyle(new NotificationCompat.BigTextStyle().bigText(""));

        Intent notificationIntent = new Intent(context, CommentActivity.class);
        // set intent so it does not start a new activity
        //notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        notificationIntent.putExtra("submittedUserId", submittedUserId);
        notificationIntent.putExtra("submittedUserName", submittedUserName);
        notificationIntent.putExtra("objectId", objectId);
        notificationIntent.putExtra("pickUpLine", pickUpLine);
        notificationIntent.putExtra("likes", likes);
        notificationIntent.putExtra("dislikes", dislikes);

        int NOTIFICATION_ID = commentText.hashCode();
        PendingIntent intent = PendingIntent.getActivity(context, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(intent);
        Notification notification = builder.build();
        //notification.bigContentView = bigView;
        notification.defaults = Notification.DEFAULT_ALL;
        notification.ledARGB = Color.GREEN;
        notification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(NOTIFICATION_ID, notification);
    }
}
