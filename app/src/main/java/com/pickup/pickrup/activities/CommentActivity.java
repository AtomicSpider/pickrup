package com.pickup.pickrup.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pickup.pickrup.R;
import com.pickup.pickrup.common.ShareVia;
import com.pickup.pickrup.common.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sanat Dutta on 2/25/2015.
 */
public class CommentActivity extends ActionBarActivity {

    private String TAG = "CommentActivity";

    //Interface
    MaterialDialog progressDialog;
    private commentsAdapter mAdapter;

    //Views
    private View positiveAction;
    private ListView mListView;
    private ProgressBarCircularIndeterminate fetchingCommentsProgressBar;
    private EditText commentEditText;
    private TextView pickUpLineTextView, pickUpLineLikeText, pickUpLineDislikeText;
    private ImageView pickUpLineBookmarkIcon, pickUpLineLikeIcon, pickUpLineDislikeIcon, pickUpLineShare, commentSend;
    private LinearLayout pickUpLineLikeClick, pickUpLineDislikeClick, noCommentLinearLayout;
    private ScrollView mScrollView;

    //Data
    private boolean isEverythingLoaded = false;
    private String submittedUserId, submittedUserName, objectId, pickUpLine;
    private int likes, dislikes;

    private boolean isDataFetching = false;
    private int COMMENTS_PER_PAGE = 20;
    private Date lastLoadedDate = null;
    private ArrayList<ParseObject> commentObjects = new ArrayList<>();

    private String reasonReport, descriptionReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "CommentActivity: onCreate()");

        getIntentData();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        setContentView(R.layout.activity_comment);
        findViews();
        setView();
        setOnClickListeners();

        mAdapter = new commentsAdapter();
        mListView.setAdapter(mAdapter);
        mListView.setScrollContainer(false);
        getComments(null);
    }

    private void getComments(final Date mDate) {
        isDataFetching = true;

        Log.i(TAG, "Fetching Comments: " + mDate);
        ParseQuery<ParseObject> commentObjectsQuery = new ParseQuery("Comments");
        commentObjectsQuery.setLimit(COMMENTS_PER_PAGE);
        commentObjectsQuery.addAscendingOrder("createdAt");
        commentObjectsQuery.whereEqualTo("pickUpLineObjectId", objectId);
        if (mDate != null) commentObjectsQuery.whereGreaterThan("createdAt", mDate);
        commentObjectsQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> commentList, ParseException e) {
                fetchingCommentsProgressBar.setVisibility(View.GONE);
                if (e == null) {
                    if (commentList.size() > 0) {
                        Log.i(TAG, "Comments Fetching Finished");
                        if (mDate == null) commentObjects.clear();
                        commentObjects.addAll(commentList);
                        lastLoadedDate = commentObjects.get(commentObjects.size() - 1).getCreatedAt();
                        mAdapter.notifyDataSetChanged();
                        setListViewHeightBasedOnChildren(mListView);
                    } else {
                        if (commentObjects.size() == 0) {
                            Log.e(TAG, "No comments");
                            noCommentLinearLayout.setVisibility(View.VISIBLE);
                        } else {
                            isEverythingLoaded = true;
                            mAdapter.notifyDataSetChanged();
                            setListViewHeightBasedOnChildren(mListView);
                        }
                    }
                } else {
                    Log.e(TAG, "Unable to fetch Comments. Error: " + e);
                    Toast.makeText(CommentActivity.this, "Unable to fetch Comments", Toast.LENGTH_SHORT).show();
                }
                isDataFetching = false;
            }
        });
    }

    private void setOnClickListeners() {

        View.OnClickListener mOnclickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.comment_pickup_line_text_view:
                        Utils.copyText(CommentActivity.this, pickUpLine);
                        break;
                    case R.id.comment_pickup_item_bookmark:
                        if (ParseUser.getCurrentUser() != null) {
                            bookmarkPickUpLine();
                        } else goToLoginActivity();
                        break;
                    case R.id.comment_pickup_item_like_click:
                        if (ParseUser.getCurrentUser() != null) {
                            likeClickPickUpLine();
                        } else
                            goToLoginActivity();
                        break;
                    case R.id.comment_pickup_item_dislike_click:
                        if (ParseUser.getCurrentUser() != null) {
                            dislikeClickPickUpLine();
                        } else
                            goToLoginActivity();
                        break;
                    case R.id.comment_pickup_item_share_icon:
                        ShareVia.findAvailableSharePackages(CommentActivity.this, pickUpLine);
                        break;
                    case R.id.comment_send:
                        postComment();
                        break;
                }
            }

            private void postComment() {
                Log.i(TAG, "Post Comment");
                final String commentText = commentEditText.getText().toString().trim();
                if (ParseUser.getCurrentUser() == null) {
                    Log.i(TAG, "User Null");
                    goToLoginActivity();
                } else {
                    Log.i(TAG, "User Not Null");
                    Boolean ErrorCode = false;
                    if (commentText.length() == 0) {
                        Log.i(TAG, "Comment Null");
                        ErrorCode = true;
                        Toast.makeText(CommentActivity.this, "Cannot post blank comment", Toast.LENGTH_SHORT).show();
                    } else if (commentText.length() > 250) {
                        Log.i(TAG, "Comment Too Large");
                        ErrorCode = true;
                        Toast.makeText(CommentActivity.this, "Comment can't have more than 250 characters", Toast.LENGTH_SHORT).show();
                    }
                    if (!ErrorCode) {
                        Log.i(TAG, "Error null. Posting Comment");
                        showProgressDialog("Posting Comment...");
                        final ParseObject commentObject = new ParseObject("Comments");
                        commentObject.put("pickUpLineObjectId", objectId);
                        commentObject.put("commentUserObjectId", ParseUser.getCurrentUser().getObjectId());
                        commentObject.put("commentUserName", ParseUser.getCurrentUser().getUsername());
                        commentObject.put("commentUserGender", ParseUser.getCurrentUser().getString("gender"));
                        commentObject.put("comment", commentText);
                        commentObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                progressDialog.dismiss();
                                if (e == null) {
                                    commentEditText.setText("");
                                    Toast.makeText(CommentActivity.this, "Comment Posted Successfully", Toast.LENGTH_SHORT).show();
                                    addCommentToListView(commentObject);
                                    Utils.increasePickUpLineCommentCount(objectId);
                                    Utils.sendCommentNotification(submittedUserId, submittedUserName, ParseUser.getCurrentUser().getObjectId(), ParseUser.getCurrentUser().getUsername(), objectId, pickUpLine, likes, dislikes, commentText);
                                    if (!submittedUserId.equals(ParseUser.getCurrentUser().getObjectId()))
                                        Utils.registerUserToPickupLineCommentList(objectId, ParseUser.getCurrentUser().getObjectId());
                                } else
                                    Toast.makeText(CommentActivity.this, "Oops ! Something went wrong. Error : " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                            private void addCommentToListView(ParseObject commentObject) {

                                commentObjects.add(commentObject);
                                noCommentLinearLayout.setVisibility(View.GONE);
                                mAdapter.notifyDataSetChanged();
                                setListViewHeightBasedOnChildren(mListView);

                                //hideKeyBoard();
                                mScrollView.fullScroll(ScrollView.FOCUS_UP);
                            }

                            private void hideKeyBoard() {
                                InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                mInputMethodManager.hideSoftInputFromWindow(commentEditText.getWindowToken(), 0);
                            }
                        });
                    }
                }
            }

            private void dislikeClickPickUpLine() {
                // if dislike array is null, create a new one
                if (MainScreenActivity.pickUpLinesDislikesArray == null)
                    MainScreenActivity.pickUpLinesDislikesArray = new ArrayList<>();

                // if dislike array does not contain the object
                if (!MainScreenActivity.pickUpLinesDislikesArray.contains(objectId)) {
                    MainScreenActivity.pickUpLinesDislikesArray.add(objectId);

                    // increase dislike count locally
                    dislikes += 1;

                    //Increment the dislike count on the pickupline object
                    ParseObject dislikeIncrement = ParseObject.createWithoutData("PickUpLines", objectId);
                    dislikeIncrement.increment("dislikes");
                    dislikeIncrement.saveInBackground();

                    if (MainScreenActivity.pickUpLinesLikesArray != null) {
                        if (MainScreenActivity.pickUpLinesLikesArray.contains(objectId)) {
                            MainScreenActivity.pickUpLinesLikesArray.remove(objectId);

                            if (likes > 0) {

                                // decrease like count locally
                                likes -= 1;

                                //Decrement the like count on the pickupline object
                                ParseObject likeDecrement = ParseObject.createWithoutData("PickUpLines", objectId);
                                likeDecrement.increment("likes", -1);
                                likeDecrement.saveInBackground();
                            }
                        }
                    }

                    // Update the User Disike Array with pickupline object id
                    ParseUser.getCurrentUser().put("pickUpLinesDislikes", MainScreenActivity.pickUpLinesDislikesArray);
                    ParseUser.getCurrentUser().saveInBackground();
                    updateView();
                }
            }

            private void likeClickPickUpLine() {
                // if like array is null, create a new one
                if (MainScreenActivity.pickUpLinesLikesArray == null)
                    MainScreenActivity.pickUpLinesLikesArray = new ArrayList<>();

                // if like array does not contain the object
                if (!MainScreenActivity.pickUpLinesLikesArray.contains(objectId)) {
                    MainScreenActivity.pickUpLinesLikesArray.add(objectId);

                    // increase like count locally
                    likes += 1;

                    //Increment the like count on the pickupline object
                    ParseObject likeIncrement = ParseObject.createWithoutData("PickUpLines", objectId);
                    likeIncrement.increment("likes");
                    likeIncrement.saveInBackground();

                    if (MainScreenActivity.pickUpLinesDislikesArray != null) {
                        if (MainScreenActivity.pickUpLinesDislikesArray.contains(objectId)) {
                            MainScreenActivity.pickUpLinesDislikesArray.remove(objectId);

                            if (dislikes > 0) {

                                // decrease dislike count locally
                                dislikes -= 1;

                                //Decrement the dislike count on the pickupline object
                                ParseObject dislikeDecrement = ParseObject.createWithoutData("PickUpLines", objectId);
                                dislikeDecrement.increment("dislikes", -1);
                                dislikeDecrement.saveInBackground();
                            }
                        }
                    }

                    // Update the User Like Array with pickupline object id
                    ParseUser.getCurrentUser().put("pickUpLinesLikes", MainScreenActivity.pickUpLinesLikesArray);
                    ParseUser.getCurrentUser().saveInBackground();
                    updateView();
                }
            }

            private void bookmarkPickUpLine() {
                if (MainScreenActivity.pickUpLinesBookmarksArray == null)
                    MainScreenActivity.pickUpLinesBookmarksArray = new ArrayList<>();

                if (MainScreenActivity.pickUpLinesBookmarksArray.contains(objectId))
                    MainScreenActivity.pickUpLinesBookmarksArray.remove(objectId);
                else
                    MainScreenActivity.pickUpLinesBookmarksArray.add(objectId);

                ParseUser.getCurrentUser().put("pickUpLinesBookmarks", MainScreenActivity.pickUpLinesBookmarksArray);
                ParseUser.getCurrentUser().saveInBackground();
                updateView();
            }
        };

        pickUpLineTextView.setOnClickListener(mOnclickListener);
        pickUpLineBookmarkIcon.setOnClickListener(mOnclickListener);
        pickUpLineLikeClick.setOnClickListener(mOnclickListener);
        pickUpLineDislikeClick.setOnClickListener(mOnclickListener);
        pickUpLineShare.setOnClickListener(mOnclickListener);
        commentSend.setOnClickListener(mOnclickListener);

    }

    private void setView() {
        getSupportActionBar().setTitle(submittedUserName);
        pickUpLineTextView.setText(pickUpLine);
        if (MainScreenActivity.pickUpLinesBookmarksArray != null) {
            if (MainScreenActivity.pickUpLinesBookmarksArray.contains(objectId))
                pickUpLineBookmarkIcon.setImageResource(R.drawable.ic_bookmark_blue_18dp);
        }
        if (MainScreenActivity.pickUpLinesLikesArray != null) {
            if (MainScreenActivity.pickUpLinesLikesArray.contains(objectId))
                pickUpLineLikeIcon.setImageResource(R.drawable.ic_like_active);
        }
        if (MainScreenActivity.pickUpLinesDislikesArray != null) {
            if (MainScreenActivity.pickUpLinesDislikesArray.contains(objectId))
                pickUpLineDislikeIcon.setImageResource(R.drawable.ic_dislike_active);
        }
        pickUpLineLikeText.setText(Utils.validateLikeDislikeCount(likes));
        pickUpLineDislikeText.setText(Utils.validateLikeDislikeCount(dislikes));
    }

    private void updateView() {
        if (MainScreenActivity.pickUpLinesBookmarksArray != null) {
            if (MainScreenActivity.pickUpLinesBookmarksArray.contains(objectId))
                pickUpLineBookmarkIcon.setImageResource(R.drawable.ic_bookmark_blue_18dp);
            else pickUpLineBookmarkIcon.setImageResource(R.drawable.ic_bookmark_grey_18dp);
        } else pickUpLineBookmarkIcon.setImageResource(R.drawable.ic_bookmark_grey_18dp);

        if (MainScreenActivity.pickUpLinesLikesArray != null) {
            if (MainScreenActivity.pickUpLinesLikesArray.contains(objectId))
                pickUpLineLikeIcon.setImageResource(R.drawable.ic_like_active);
            else pickUpLineLikeIcon.setImageResource(R.drawable.ic_like_inactive);
        } else pickUpLineLikeIcon.setImageResource(R.drawable.ic_like_inactive);

        if (MainScreenActivity.pickUpLinesDislikesArray != null) {
            if (MainScreenActivity.pickUpLinesDislikesArray.contains(objectId))
                pickUpLineDislikeIcon.setImageResource(R.drawable.ic_dislike_active);
            else pickUpLineDislikeIcon.setImageResource(R.drawable.ic_dislike_inactive);
        } else pickUpLineDislikeIcon.setImageResource(R.drawable.ic_dislike_inactive);

        pickUpLineLikeText.setText(Utils.validateLikeDislikeCount(likes));
        pickUpLineDislikeText.setText(Utils.validateLikeDislikeCount(dislikes));
    }

    private void getIntentData() {
        submittedUserId = getIntent().getStringExtra("submittedUserId");
        submittedUserName = getIntent().getStringExtra("submittedUserName");
        objectId = getIntent().getStringExtra("objectId");
        pickUpLine = getIntent().getStringExtra("pickUpLine");
        likes = getIntent().getIntExtra("likes", 0);
        dislikes = getIntent().getIntExtra("dislikes", 0);
    }

    private void findViews() {
        pickUpLineTextView = (TextView) findViewById(R.id.comment_pickup_line_text_view);
        pickUpLineBookmarkIcon = (ImageView) findViewById(R.id.comment_pickup_item_bookmark);
        pickUpLineLikeClick = (LinearLayout) findViewById(R.id.comment_pickup_item_like_click);
        pickUpLineDislikeClick = (LinearLayout) findViewById(R.id.comment_pickup_item_dislike_click);
        pickUpLineLikeIcon = (ImageView) findViewById(R.id.comment_pickup_item_like_icon);
        pickUpLineDislikeIcon = (ImageView) findViewById(R.id.comment_pickup_item_dislike_icon);
        pickUpLineLikeText = (TextView) findViewById(R.id.comment_pickup_item_like_text);
        pickUpLineDislikeText = (TextView) findViewById(R.id.comment_pickup_item_dislike_text);
        pickUpLineShare = (ImageView) findViewById(R.id.comment_pickup_item_share_icon);
        commentEditText = (EditText) findViewById(R.id.comment_comment_edit_text);
        commentSend = (ImageView) findViewById(R.id.comment_send);

        mListView = (ListView) findViewById(R.id.comment_list_view);
        fetchingCommentsProgressBar = (ProgressBarCircularIndeterminate) findViewById(R.id.fetching_comments_progressbar);

        noCommentLinearLayout = (LinearLayout) findViewById(R.id.noCommentLinearLayout);
        mScrollView = (ScrollView) findViewById(R.id.comment_scroll_view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_comment_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_report_pickupline:
                showReportPickuplineDialog();
                break;
        }
        return true;
    }

    private void showReportPickuplineDialog() {
        reasonReport = "Spam";
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Report Pickupline")
                .titleColorRes(R.color.teal_700)
                .customView(R.layout.dialog_report, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        reportUser();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        RadioGroup radioGroupInput = (RadioGroup) dialog.getCustomView().findViewById(R.id.dialog_report_radio_group);
        radioGroupInput.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.spamRadioButton:
                        reasonReport = "Spam";
                        break;
                    case R.id.offensiveRadioButton:
                        reasonReport = "Offensive";
                        break;
                    case R.id.otherRadioButton:
                        reasonReport = "Other";
                        break;
                }
            }
        });
        EditText descriptionInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_report_desc);
        descriptionInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    descriptionReport = s.toString().trim();
                    positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    private void reportUser() {
        showProgressDialog("Sending Message...");
        ParseObject reportPickupline = new ParseObject("ReportedPickUpLines");
        reportPickupline.put("submittedUserId", submittedUserId);
        reportPickupline.put("submittedUserName", submittedUserName);
        reportPickupline.put("pickUpLineId", objectId);
        reportPickupline.put("pickUpLine", pickUpLine);
        reportPickupline.put("reason", reasonReport);
        reportPickupline.put("description", descriptionReport);
        if (ParseUser.getCurrentUser() != null) {
            reportPickupline.put("senderObjectId", ParseUser.getCurrentUser().getObjectId());
            reportPickupline.put("senderUserName", ParseUser.getCurrentUser().getUsername());
            reportPickupline.put("senderGender", ParseUser.getCurrentUser().getString("gender"));
            reportPickupline.put("senderAge", ParseUser.getCurrentUser().getString("age"));
        }
        reportPickupline.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    Toast.makeText(CommentActivity.this, "Report Sent", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CommentActivity.this, "Oops ! Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void goToLoginActivity() {
        Intent goToLoginActivityIntent = new Intent(CommentActivity.this, LoginActivity.class);
        startActivity(goToLoginActivityIntent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(CommentActivity.this)
                .content(message)
                .contentColorRes(R.color.teal_700)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private class commentsAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;

        private commentsAdapter() {
            mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return commentObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return commentObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View mView;
            if ((position == (commentObjects.size() - 1)) && (commentObjects.size() > COMMENTS_PER_PAGE - 1) && !isEverythingLoaded)
                mView = mLayoutInflater.inflate(R.layout.item_comment_last, parent, false);
            else mView = mLayoutInflater.inflate(R.layout.item_comment, parent, false);
            final ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.commentItemUsername = (TextView) mView.findViewById(R.id.comment_item_username);
            mViewHolder.commentItemTime = (TextView) mView.findViewById(R.id.comment_item_time);
            mViewHolder.commentLineTextView = (TextView) mView.findViewById(R.id.comment_line_text_view);

            mViewHolder.commentItemUsername.setText(commentObjects.get(position).getString("commentUserName"));
            mViewHolder.commentItemTime.setText(Utils.getTimeDifference(commentObjects.get(position).getCreatedAt()));
            mViewHolder.commentLineTextView.setText(commentObjects.get(position).getString("comment"));

            View.OnClickListener mOnclickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.comment_item_username:
                            goToProfileViewActivity();
                            break;
                    }
                }

                private void goToProfileViewActivity() {
                    Intent goToProfileViewActivityIntent = new Intent(CommentActivity.this, UserProfileViewActivity.class);
                    goToProfileViewActivityIntent.putExtra("userObjectId", commentObjects.get(position).getString("commentUserObjectId"));
                    goToProfileViewActivityIntent.putExtra("userName", commentObjects.get(position).getString("commentUserName"));
                    startActivity(goToProfileViewActivityIntent);
                }
            };

            mViewHolder.commentItemUsername.setOnClickListener(mOnclickListener);

            // Fetch more data
            if ((position == (commentObjects.size() - 1)) && !isDataFetching && !isEverythingLoaded) {
                Log.i(TAG, "Fetching comment last view");
                getComments(lastLoadedDate);
            }

            return mView;
        }

        private class ViewHolder {
            private TextView commentItemUsername;
            private TextView commentItemTime;
            private TextView commentLineTextView;
        }
    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int totalHeight = 0;
        int numberOfItems = listAdapter.getCount();
        View view;
        for (int i = 0; i < numberOfItems; i++) {
            view = listAdapter.getView(i, null, listView);
            view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            totalHeight += view.getMeasuredHeight();
        }
        totalHeight +=40;
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
