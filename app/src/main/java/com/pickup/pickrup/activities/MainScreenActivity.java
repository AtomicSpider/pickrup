package com.pickup.pickrup.activities;// Created by Sanat Dutta on 2/17/2015.

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pickup.pickrup.PickrUpApp;
import com.pickup.pickrup.R;
import com.pickup.pickrup.common.RateTheApp;
import com.pickup.pickrup.fragments.FavoritePickupLinesFragment;
import com.pickup.pickrup.fragments.RecentPickupLinesFragment;
import com.pickup.pickrup.fragments.TopPickupLinesFragment;
import com.pickup.pickrup.tabbarview.TabBarView;

import java.util.ArrayList;
import java.util.List;

import static com.pickup.pickrup.common.RateTheApp.setOptOut;

public class MainScreenActivity extends ActionBarActivity {

    private String TAG = "MainScreenActivity";

    //Interfaces
    private Handler mHandler = new Handler();
    private Toolbar mToolbar;
    private TabBarView mTabBarView;
    private MainScreenPagerAdapter mMainScreenPagerAdapter;
    private ViewPager mViewPager;
    private ActionBarDrawerToggle mDrawerToggle;
    private IntentFilter mFilter = null;

    //Views
    private RelativeLayout navigationDrawerProfileView;
    private MaterialDialog progressDialog;
    private View positiveAction;
    private DrawerLayout mDrawerLayout;
    private FloatingActionButton submitButton;
    private TextView navUserName, navUserRank, navLoginTextView, navMyLinesTextView, navAboutTextView, navDonateTextView, navRateTextView, navFeedbackTextView, navLogoutTextView;
    private ImageView navProfileImage;

    //Data
    private boolean EXIT_DURATION = false;
    private String feedbackDialogSubject, feedbackDialogMessage;
    private int PAGE_COUNT = 3;
    public static ArrayList<String> pickUpLinesBookmarksArray = new ArrayList<>();
    public static ArrayList<String> pickUpLinesLikesArray = new ArrayList<>();
    public static ArrayList<String> pickUpLinesDislikesArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Fetch user data
        if (ParseUser.getCurrentUser() != null) fetchUserSavedArrays(true);
        else fetchUserSavedArrays(false);

        Log.i(TAG, "MainScreenActivity: onCreate()");

        setContentView(R.layout.activity_main_screen);
        findViews();

        setUserNavigationView();

        setUpCustomTabs();
        setPagerListener();
        setUpNavigationDrawer();
        setUpNavigationItems();
        setOnClickListeners();
        setAppRate();
        registerForBroadcasts();

        checkIfTheUserIsBanned();

        getPickupLineCount();
        saveInstallationToParse();
        toggleCommentNotificationChannel();
    }

    private void toggleCommentNotificationChannel() {

        Log.i(TAG, "toggleCommentNotificationChannel");

        if (ParseUser.getCurrentUser() != null) {
            SharedPreferences pref = getSharedPreferences("PREF_SETTING", Context.MODE_PRIVATE);
            if (pref.getBoolean("RECEIVE_NOTIFICATION", true))
                ParsePush.subscribeInBackground("commentNotification");
            else
                ParsePush.unsubscribeInBackground("commentNotification");
        }
        else ParsePush.unsubscribeInBackground("commentNotification");
    }

    private void saveInstallationToParse() {
        ParseInstallation mParseInstallation = ParseInstallation.getCurrentInstallation();
        if (ParseUser.getCurrentUser() != null)
            mParseInstallation.put("deviceId", ParseUser.getCurrentUser().getObjectId());
        else mParseInstallation.put("deviceId", "");
        mParseInstallation.saveInBackground();
    }

    private void getPickupLineCount() {
        if (ParseUser.getCurrentUser() != null) {
            ParseQuery<ParseObject> getPickupLineCount = ParseQuery.getQuery("PickUpLines");
            getPickupLineCount.whereEqualTo("submittedUserId", ParseUser.getCurrentUser().getObjectId());
            getPickupLineCount.countInBackground(new CountCallback() {
                public void done(int count, ParseException e) {
                    if (e == null) {
                        Log.e(TAG, "Pickup Line Count: " + count);
                        updateRankingSystem(count);
                    } else {
                        Log.e(TAG, "Could not fetch count, Error: " + e.getMessage());
                    }
                }

                private void updateRankingSystem(int count) {
                    String currentRank = ParseUser.getCurrentUser().getString("level");
                    String newRank = currentRank;
                    if (count < 50) newRank = "PickupNoob";
                    else if ((count >= 50) && (count < 100)) newRank = "PickupRookie";
                    else if ((count >= 100) && (count < 200)) newRank = "PickupArtist";
                    else if ((count >= 200) && (count < 400)) newRank = "PickupKnight";
                    else if ((count >= 400) && (count < 700)) newRank = "PickupKing";
                    else if ((count >= 700) && (count < 1000)) newRank = "PickupMaster";
                    else if (count >= 1000) newRank = "PickupWizard";

                    if (!currentRank.equals(newRank)) {
                        ParseUser currentUser = ParseUser.getCurrentUser();
                        currentUser.put("level", newRank);
                        currentUser.saveInBackground();
                        navUserRank.setText(newRank);
                        showRankUnlockedDialog(newRank);
                    }
                }
            });
        }
    }

    private void showRankUnlockedDialog(String newRank) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Leveled Up !!!")
                .titleColorRes(R.color.teal_700)
                .content("Congratulations ! You are now a " + newRank)
                .positiveText("Awesome")
                .cancelable(false)
                .build();

        dialog.show();
    }

    private void setUserNavigationView() {
        if (ParseUser.getCurrentUser() != null) {
            if (ParseUser.getCurrentUser().getString("gender").equals("Female"))
                navProfileImage.setImageResource(R.drawable.ic_profile_girl_blue);
            navUserName.setText(ParseUser.getCurrentUser().getUsername());
            navUserRank.setText(ParseUser.getCurrentUser().getString("level"));
        }
    }

    private void registerForBroadcasts() {
        mFilter = new IntentFilter(PickrUpApp.ACTION_LOGOUT);
        registerReceiver(mReceiver, mFilter);
    }

    private void checkIfTheUserIsBanned() {
        if (ParseUser.getCurrentUser() != null) {
            Log.i(TAG, "User not null, Checking for user setup class");
            ParseQuery userStatusQuery = new ParseQuery("UserStatus");
            userStatusQuery.whereEqualTo("userObjectId", ParseUser.getCurrentUser().getObjectId());
            userStatusQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    if (list.size() == 0) {

                        Log.i(TAG, "User status class was not setup, Setting it up");
                        ParseObject userStatusObject = new ParseObject("UserStatus");
                        userStatusObject.put("userObjectId", ParseUser.getCurrentUser().getObjectId());
                        userStatusObject.put("userName", ParseUser.getCurrentUser().getUsername());
                        userStatusObject.put("gender", ParseUser.getCurrentUser().getString("gender"));
                        userStatusObject.put("age", ParseUser.getCurrentUser().getString("age"));
                        userStatusObject.put("banned", false);
                        userStatusObject.put("deactivated", false);

                        userStatusObject.saveInBackground();
                    } else {
                        Log.i(TAG, "User status class is setup");
                        if (list.get(0).getBoolean("banned")) {
                            Log.i(TAG, "User banned, Logging Out");
                            showBannedDialog();
                        } else Log.i(TAG, "User status is okay, Exiting");
                    }
                }
            });
        } else Log.i(TAG, "User null, Exiting");
    }

    private void showBannedDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Account Banned")
                .titleColorRes(R.color.teal_700)
                .content("Your account has been banned for violating rules, you can contact support@pickrup.com for further enquiry")
                .positiveText("Okay")
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Log.i(TAG, "Okay");
                        if (ParseUser.getCurrentUser() != null) ParseUser.logOut();
                        recreate();
                    }
                }).build();

        dialog.show();
    }

    private void setAppRate() {
        RateTheApp.onStart(this);
        RateTheApp.showRateDialogIfNeeded(this);
    }

    private void findViews() {
        submitButton = (FloatingActionButton) findViewById(R.id.submitMain);
        navLoginTextView = (TextView) findViewById(R.id.nav_item_login);
        navMyLinesTextView = (TextView) findViewById(R.id.nav_item_my_lines);
        navAboutTextView = (TextView) findViewById(R.id.nav_item_about);
        navDonateTextView = (TextView) findViewById(R.id.nav_item_donate);
        navRateTextView = (TextView) findViewById(R.id.nav_item_rate);
        navFeedbackTextView = (TextView) findViewById(R.id.nav_item_feedback);
        navLogoutTextView = (TextView) findViewById(R.id.nav_item_logout);

        navUserName = (TextView) findViewById(R.id.navigation_drawer_userName);
        navUserRank = (TextView) findViewById(R.id.navigation_drawer_userLevel);
        navProfileImage = (ImageView) findViewById(R.id.navigation_drawer_profileImage);
        navigationDrawerProfileView = (RelativeLayout) findViewById(R.id.navigation_drawer_profileView);
    }

    private void setUpNavigationItems() {
        if (ParseUser.getCurrentUser() != null) {
            navLoginTextView.setVisibility(View.GONE);
            navMyLinesTextView.setVisibility(View.VISIBLE);
            navLogoutTextView.setVisibility(View.VISIBLE);
        } else {
            navLoginTextView.setVisibility(View.VISIBLE);
            navMyLinesTextView.setVisibility(View.GONE);
            navLogoutTextView.setVisibility(View.GONE);
        }
    }

    private void setOnClickListeners() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ParseUser.getCurrentUser() != null) {
                    Intent mIntent = new Intent(MainScreenActivity.this, SubmitActivity.class);
                    startActivity(mIntent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_bottom);
                } else goToLoginActivity();
            }
        });
        navLoginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                goToLoginActivity();
            }
        });
        navMyLinesTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                Intent mIntent = new Intent(MainScreenActivity.this, UserPickupLinesActivity.class);
                mIntent.putExtra("USER_ID", ParseUser.getCurrentUser().getObjectId());
                mIntent.putExtra("USER_NAME", ParseUser.getCurrentUser().getUsername());
                startActivity(mIntent);
            }
        });
        navAboutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                showAboutDialog();
            }
        });
        navDonateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                Intent mIntent = new Intent(MainScreenActivity.this, DonateActivity.class);
                startActivity(mIntent);
            }
        });
        navRateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                String appPackage = getPackageName();
                Intent mIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackage));
                startActivity(mIntent);
                setOptOut(MainScreenActivity.this, true);
            }
        });
        navFeedbackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                showFeedbackDialog();
            }
        });
        navLogoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ParseUser.getCurrentUser() != null) ParseUser.logOut();
                recreate();
            }
        });
        navigationDrawerProfileView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(Gravity.END);
                if (ParseUser.getCurrentUser() != null) {
                    Intent goToProfileViewActivityIntent = new Intent(MainScreenActivity.this, UserProfileViewActivity.class);
                    goToProfileViewActivityIntent.putExtra("userObjectId", ParseUser.getCurrentUser().getObjectId());
                    goToProfileViewActivityIntent.putExtra("userName", ParseUser.getCurrentUser().getUsername());
                    startActivity(goToProfileViewActivityIntent);
                } else goToLoginActivity();
            }
        });
    }

    private void showAboutDialog() {
        String mData = "<html><body>" +
                "<p>PickrUp’s mission is to give people the power to share pickup lines and also to create a simple interface to share pickup lines across different social networks." +
                "<br/><br/>Website" +
                "<br/><a href=\"http://www.pickrup.com/\">www.pickrup.com</a>" +
                "<br/><br/>The app is developed on a budget of absolutely nothing. This wouldn't have been possible without some awesome people and organizations. List of resources can be seen <a href=\"http://www.pickrup.com/credits.html\">here</a>" +
                "<br/><br/>Designed and developed by <br/>Sanat Dutta" +
                "</p>" + "</body></html>";

        /*TextView mMessageTextView = (TextView) mDialog.findViewById(android.R.id.message);
        mMessageTextView.setMovementMethod(LinkMovementMethod.getInstance());
        mMessageTextView.setTextSize(16);*/

        new MaterialDialog.Builder(this)
                .title("About")
                .titleColorRes(R.color.teal_700)
                .content(Html.fromHtml(mData))
                .positiveText("Close")
                .build()
                .show();
    }

    private void showFeedbackDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Feedback")
                .titleColorRes(R.color.teal_700)
                .customView(R.layout.dialog_feedback, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        sendFeedback();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);

        final EditText subjectInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_feedback_subject);
        subjectInput.requestFocus();
        final EditText messageInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_feedback_message);

        subjectInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    feedbackDialogSubject = s.toString().trim();
                    if (messageInput.getText().toString().trim().length() > 0)
                        positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        messageInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    feedbackDialogMessage = s.toString().trim();
                    if (subjectInput.getText().toString().trim().length() > 0)
                        positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    private void sendFeedback() {
        showProgressDialog("Sending Feedback...");
        String versionName = "N/A";
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        ParseObject feedback = new ParseObject("Feedback");
        feedback.put("appVersion", versionName);
        feedback.put("subject", feedbackDialogSubject);
        feedback.put("message", feedbackDialogMessage);
        if (ParseUser.getCurrentUser() != null) {
            feedback.put("senderObjectId", ParseUser.getCurrentUser().getObjectId());
            feedback.put("senderUserName", ParseUser.getCurrentUser().getUsername());
            feedback.put("senderGender", ParseUser.getCurrentUser().getString("gender"));
            feedback.put("senderAge", ParseUser.getCurrentUser().getString("age"));
        } else {
            feedback.put("senderObjectId", "N/A");
            feedback.put("senderUserName", "N/A");
            feedback.put("senderGender", "N/A");
            feedback.put("senderAge", "N/A");
        }
        feedback.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    Toast.makeText(MainScreenActivity.this, "Feedback Sent", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainScreenActivity.this, "Oops ! Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.teal_700)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private void goToLoginActivity() {
        Intent goToLoginActivityIntent = new Intent(MainScreenActivity.this, LoginActivity.class);
        startActivity(goToLoginActivityIntent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void setUpNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.d(TAG, "Drawer Opened");
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d(TAG, "Drawer Closed");
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void fetchUserSavedArrays(Boolean isUserPresent) {
        if (isUserPresent) {
            pickUpLinesBookmarksArray = (ArrayList<String>) ParseUser.getCurrentUser().get("pickUpLinesBookmarks");
            pickUpLinesLikesArray = (ArrayList<String>) ParseUser.getCurrentUser().get("pickUpLinesLikes");
            pickUpLinesDislikesArray = (ArrayList<String>) ParseUser.getCurrentUser().get("pickUpLinesDislikes");

            if (pickUpLinesBookmarksArray == null) pickUpLinesBookmarksArray = new ArrayList<>();
            if (pickUpLinesLikesArray == null) pickUpLinesLikesArray = new ArrayList<>();
            if (pickUpLinesDislikesArray == null) pickUpLinesDislikesArray = new ArrayList<>();
        } else {
            pickUpLinesBookmarksArray = new ArrayList<>();
            pickUpLinesLikesArray = new ArrayList<>();
            pickUpLinesDislikesArray = new ArrayList<>();
        }
    }

    private void setUpCustomTabs() {
        LayoutInflater mLayoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View customTabView = mLayoutInflater.inflate(R.layout.custom_tab_view, null);
        mTabBarView = (TabBarView) customTabView.findViewById(R.id.customTabBar);
        mTabBarView.setStripHeight(5);
        mTabBarView.setStripColor(getResources().getColor(R.color.white));

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.addView(customTabView);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

        mMainScreenPagerAdapter = new MainScreenPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mMainScreenPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mTabBarView.setViewPager(mViewPager);
    }

    private void setPagerListener() {
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mTabBarView.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                Log.i(TAG, "Page: " + position);
                mTabBarView.onPageSelected(position);

                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                mTabBarView.onPageScrollStateChanged(state);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_drawer_toggle) {
            if (mDrawerLayout.isDrawerOpen(Gravity.END)) mDrawerLayout.closeDrawer(Gravity.END);
            else mDrawerLayout.openDrawer(Gravity.END);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDrawerLayout.isDrawerOpen(Gravity.END)) mDrawerLayout.closeDrawer(Gravity.END);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private void showLoginDialog() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        LayoutInflater mLayoutInflater = getLayoutInflater();
        View mLayout = mLayoutInflater.inflate(R.layout.dialog_login, null);
        mBuilder.setView(mLayout);
        final EditText usernameInput = (EditText) mLayout.findViewById(R.id.login_user_name);
        final EditText passwordInput = (EditText) mLayout.findViewById(R.id.login_password);
        mBuilder.setTitle("Login");
        mBuilder.setPositiveButton("LOGIN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ParseUser.logInInBackground(usernameInput.getText().toString().trim(), passwordInput.getText().toString().trim(), new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        if (e == null)
                            Toast.makeText(MainScreenActivity.this, "Logged In", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        mBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    public class MainScreenPagerAdapter extends FragmentPagerAdapter implements TabBarView.IconTabProvider {

        private int[] tab_icons = {R.drawable.ic_tab_recent_white,
                R.drawable.ic_tab_lines_white,
                R.drawable.ic_tab_fav_white,
                R.drawable.ic_tab_recent_teal_900,
                R.drawable.ic_tab_lines_teal_900,
                R.drawable.ic_tab_fav_teal_900};

        public MainScreenPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return new RecentPickupLinesFragment();
                case 1:
                    return new TopPickupLinesFragment();
                case 2:
                    return new FavoritePickupLinesFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public int getPageIconResId(int position) {
            return tab_icons[position];
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Recent";
                case 1:
                    return "Top";
                case 2:
                    return "Favorite";
            }
            return null;
        }
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(PickrUpApp.ACTION_LOGOUT)) {
                showBannedDialog();
            }
        }
    };

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.END)) mDrawerLayout.closeDrawer(Gravity.END);
        else {
            if (EXIT_DURATION) finish();
            else {
                EXIT_DURATION = true;
                mHandler.postDelayed(inValidateExitDuration, 2000);
                Toast.makeText(MainScreenActivity.this, "Press Back Again To Exit", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private Runnable inValidateExitDuration = new Runnable() {
        @Override
        public void run() {
            EXIT_DURATION = false;
        }
    };
}