package com.pickup.pickrup.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.parse.SaveCallback;
import com.pickup.pickrup.PickrUpApp;
import com.pickup.pickrup.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sanat Dutta on 3/5/2015.
 */
public class EmailLoginActivity extends ActionBarActivity {

    private String TAG = "EmailLoginActivity";

    //Interface
    MaterialDialog progressDialog, mVerifyDialog;

    //Views
    private EditText usernameEditText, passwordEditText;
    private TextView loginButton, forgotPassword;
    private View positiveAction;

    //Data
    private String emailEditText;
    private boolean isUnverifiedUserPresent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "EmailLoginActivity: onCreate()");
        setContentView(R.layout.activity_email_login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        findViews();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String errorString = null;
                if (passwordEditText.getText().toString().equals("")) {
                    errorString = "Password field is empty";
                }
                if (passwordEditText.getText().toString().contains(" ")) {
                    errorString = "Password cannot contain blank spaces";
                }
                if (usernameEditText.getText().toString().equals("")) {
                    errorString = "Username field is empty";
                }
                if (usernameEditText.getText().toString().contains(" ")) {
                    errorString = "Username cannot contain blank spaces";
                }

                if (errorString == null) {
                    loginParse(usernameEditText.getText().toString(), passwordEditText.getText().toString());
                } else
                    Toast.makeText(EmailLoginActivity.this, "" + errorString, Toast.LENGTH_SHORT).show();
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showResetDialog();
            }
        });
    }

    private void showResetDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Password Reset")
                .titleColorRes(R.color.teal_700)
                .customView(R.layout.dialog_reset_password, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        showProgressDialog("Sending Email...");
                        ParseUser.requestPasswordResetInBackground(emailEditText ,
                                new RequestPasswordResetCallback() {
                                    public void done(ParseException e) {
                                        progressDialog.dismiss();
                                        if (e == null) {
                                            Toast.makeText(EmailLoginActivity.this, "Check your email for reset instructions", Toast.LENGTH_LONG).show();
                                        } else {
                                            Log.i(TAG, ""+e+" "+e.getCode());
                                            if (e.getCode() == PickrUpApp.RESET_PASSWORD_EMAIL_NOT_FOUND) Toast.makeText(EmailLoginActivity.this, "Entered email is not registered", Toast.LENGTH_LONG).show();
                                            else Toast.makeText(EmailLoginActivity.this, ""+e.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        EditText emailInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_reset_email);
        emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isEmailValid(s.toString().trim())) {
                    emailEditText = s.toString().trim();
                    positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    public boolean isEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    private void loginParse(String userName, String passWord) {
        showProgressDialog("Logging In...");
        ParseUser.logInInBackground(userName, passWord, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    if (parseUser.getBoolean("emailVerified")) {
                        isUnverifiedUserPresent = false;
                        enableCommentNotification();
                        closeAllAndGoToMain();
                    }
                    else {
                        isUnverifiedUserPresent = true;
                        showVerifyEmailDialog();
                    }
                } else {
                    if (e.getCode() == PickrUpApp.INVALID_LOGIN_CREDENTIALS) Toast.makeText(EmailLoginActivity.this, "Email or Password is incorrect", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(EmailLoginActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void enableCommentNotification() {
        SharedPreferences pref = getSharedPreferences("PREF_SETTING", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("RECEIVE_NOTIFICATION", true);
        editor.commit();
    }

    private void showVerifyEmailDialog() {
        mVerifyDialog = new MaterialDialog.Builder(this)
                .title("Account Not Verified")
                .titleColorRes(R.color.teal_700)
                .content("Your email: " + ParseUser.getCurrentUser().getEmail() + " is not verified. Please check your email for the verification email. If you didn't receive an verification email, click on resend.")
                .positiveText("Okay")
                .cancelable(false)
                .neutralText("Resend")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        Log.i(TAG, "Resend");
                        ParseUser currentUser = ParseUser.getCurrentUser();
                        currentUser.setEmail(currentUser.getEmail());
                        showProgressDialog("Sending Email...");
                        currentUser.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                ParseUser.logOut();
                                progressDialog.dismiss();
                                if (e == null)
                                    Toast.makeText(EmailLoginActivity.this, "Verification Email Sent", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(EmailLoginActivity.this, "Could not send verification email", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Log.i(TAG, "Okay");
                        ParseUser.logOut();
                    }
                }).build();

        mVerifyDialog.show();
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.teal_700)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private void closeAllAndGoToMain() {
        Intent mIntent = new Intent(EmailLoginActivity.this, MainScreenActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mIntent);
    }

    private void findViews() {
        usernameEditText = (EditText) findViewById(R.id.email_login_userNameEditText);
        passwordEditText = (EditText) findViewById(R.id.email_login_passWordEditText);
        loginButton = (TextView) findViewById(R.id.email_login_loginButton);
        forgotPassword = (TextView) findViewById(R.id.email_login_forgotPassword);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        if (mVerifyDialog!=null) {
            if (mVerifyDialog.isShowing()) mVerifyDialog.dismiss();
        }

        if (isUnverifiedUserPresent) {
            Log.i(TAG, "Unverified user logged out");
            ParseUser.logOut();
        }
        super.onPause();
    }
}
