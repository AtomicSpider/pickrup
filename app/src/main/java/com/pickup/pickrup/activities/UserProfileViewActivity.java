package com.pickup.pickrup.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pickup.pickrup.R;

import java.util.List;

/**
 * Created by Sanat Dutta on 2/26/2015.
 */
public class UserProfileViewActivity extends ActionBarActivity {

    private String TAG = "UserProfileViewActivity";

    //Views
    private MaterialDialog progressDialog;
    private View positiveAction;
    private LinearLayout userProfileViewLayout;
    private ProgressBarCircularIndeterminate userProfileViewProgressBar;
    private RelativeLayout bannedUserView, settingRelLayout;
    private ImageView userProfilePictureImageView;
    private TextView userNameTextView, levelTextView, tagLineTextView, locationTextView, genderTextView, ageTextView, pickUpLineCount, viewPickUpLines;
    private CheckBox commentNotificationCheckBox;

    //Data
    private String PREF_SETTING = "PREF_SETTING";
    private ParseUser userObject;
    private String userObjectId;
    private final int REPORT_USER_ID = 0, EDIT_MY_PROFILE = 1;
    private boolean isUserPresent = false;
    private String reasonReport, descriptionReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "onCreate()");

        getIntentData();
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_user_profile_view);
        findViews();
        getPickupLineCount();
        fetchUserData();
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        viewPickUpLines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(UserProfileViewActivity.this, UserPickupLinesActivity.class);
                mIntent.putExtra("USER_ID", userObjectId);
                mIntent.putExtra("USER_NAME", userObject.getString("username"));
                startActivity(mIntent);
            }
        });
        commentNotificationCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCommentNotificationPref(commentNotificationCheckBox.isChecked());
            }

            private void toggleCommentNotificationPref(boolean checked) {
                SharedPreferences pref = getSharedPreferences(PREF_SETTING, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("RECEIVE_NOTIFICATION", checked);
                editor.commit();

                if (checked) {
                    ParsePush.subscribeInBackground("commentNotification");
                    Toast.makeText(UserProfileViewActivity.this, "Notifications Enabled", Toast.LENGTH_SHORT).show();
                } else {
                    ParsePush.unsubscribeInBackground("commentNotification");
                    Toast.makeText(UserProfileViewActivity.this, "Notifications Disabled", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getPickupLineCount() {
        ParseQuery<ParseObject> getPickupLineCount = ParseQuery.getQuery("PickUpLines");
        getPickupLineCount.whereEqualTo("submittedUserId", userObjectId);
        getPickupLineCount.countInBackground(new CountCallback() {
            public void done(int count, ParseException e) {
                if (e == null) {
                    Log.e(TAG, "Pickup Line Count: " + count);
                    pickUpLineCount.setText("" + count);
                } else {
                    Log.e(TAG, "Could not fetch count, Error: " + e.getMessage());
                }
            }
        });
    }

    private void fetchUserData() {
        ParseQuery userQuery = ParseUser.getQuery();
        userQuery.whereEqualTo("objectId", userObjectId);
        userQuery.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> userList, ParseException e) {
                if (e == null) {
                    if (userList.size() > 0) {
                        isUserPresent = true;
                        userObject = userList.get(0);
                        userProfileViewProgressBar.setVisibility(View.INVISIBLE);
                        displayUserDetails();
                        invalidateOptionsMenu();
                        toggleCommentNotificationCheckBoxView();
                    } else {
                        Log.i(TAG, "User Does Not Exist");
                        userProfileViewProgressBar.setVisibility(View.INVISIBLE);
                        bannedUserView.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.e(TAG, "Can't find the user. Error: " + e);
                    userProfileViewProgressBar.setVisibility(View.INVISIBLE);
                    bannedUserView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void toggleCommentNotificationCheckBoxView() {
        if (isUserPresent) {
            if (ParseUser.getCurrentUser() != null) {
                if (userObjectId.equals(ParseUser.getCurrentUser().getObjectId()))
                    setCheckBox();
            }
        }
    }

    private void setCheckBox() {
        SharedPreferences pref = getSharedPreferences(PREF_SETTING, Context.MODE_PRIVATE);
        if (pref.getBoolean("RECEIVE_NOTIFICATION", true))
            commentNotificationCheckBox.setChecked(true);
        else
            commentNotificationCheckBox.setChecked(false);

        settingRelLayout.setVisibility(View.VISIBLE);
    }

    private void displayUserDetails() {
        if (userObject.getString("username") != null) {
            userNameTextView.setText(userObject.getString("username"));
            if (ParseUser.getCurrentUser() != null) {
                if (ParseUser.getCurrentUser().getObjectId().equals(userObjectId))
                    viewPickUpLines.setText("My Pickup Lines");
                else
                    viewPickUpLines.setText("View " + userObject.getString("username") + "\'s Pickup Lines");
            } else
                viewPickUpLines.setText("View " + userObject.getString("username") + "\'s Pickup Lines");
        }
        if (userObject.getString("level") != null)
            levelTextView.setText(userObject.getString("level"));
        if (userObject.getString("tagLine") != null && !userObject.getString("tagLine").equals("")) {
            tagLineTextView.setVisibility(View.VISIBLE);
            tagLineTextView.setText("\"" + userObject.getString("tagLine") + "\"");
        } else tagLineTextView.setVisibility(View.GONE);
        if (userObject.getString("location") != null && !userObject.getString("location").equals("")) {
            locationTextView.setText(userObject.getString("location"));
            locationTextView.setVisibility(View.VISIBLE);
        } else locationTextView.setVisibility(View.GONE);
        if (userObject.getString("gender") != null) {
            genderTextView.setText(", " + userObject.getString("gender"));
            if (userObject.getString("gender").equals("Female"))
                userProfilePictureImageView.setImageResource(R.drawable.ic_profile_girl_blue);
        } else genderTextView.setText("");
        if (userObject.getString("age") != null)
            ageTextView.setText(", " + userObject.getString("age"));
        else ageTextView.setText("");

        userProfileViewLayout.setVisibility(View.VISIBLE);
    }

    private void findViews() {
        userProfileViewLayout = (LinearLayout) findViewById(R.id.user_profile_view_layout);
        userProfileViewProgressBar = (ProgressBarCircularIndeterminate) findViewById(R.id.user_profile_view_progressbar);
        userProfilePictureImageView = (ImageView) findViewById(R.id.user_profile_view_profile_picture);
        userNameTextView = (TextView) findViewById(R.id.user_profile_view_username);
        levelTextView = (TextView) findViewById(R.id.user_profile_view_level);
        tagLineTextView = (TextView) findViewById(R.id.user_profile_view_tagline);
        locationTextView = (TextView) findViewById(R.id.user_profile_view_location);
        genderTextView = (TextView) findViewById(R.id.user_profile_view_gender);
        ageTextView = (TextView) findViewById(R.id.user_profile_view_age);
        bannedUserView = (RelativeLayout) findViewById(R.id.user_profile_view_banned);
        pickUpLineCount = (TextView) findViewById(R.id.pickUpLineCount);
        viewPickUpLines = (TextView) findViewById(R.id.viewPickUpLines);
        settingRelLayout = (RelativeLayout) findViewById(R.id.settingRelLayout);
        commentNotificationCheckBox = (CheckBox) findViewById(R.id.commentNotificationCheckBox);
    }

    private void getIntentData() {
        userObjectId = getIntent().getStringExtra("userObjectId");
        if (userObjectId == null) finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_user_profile_view, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (isUserPresent) {
            if (ParseUser.getCurrentUser() != null) {
                if (userObjectId.equals(ParseUser.getCurrentUser().getObjectId()))
                    menu.add(0, EDIT_MY_PROFILE, 0, "Edit Profile").setIcon(R.drawable.ic_edit_white_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                else
                    menu.add(0, REPORT_USER_ID, 0, "Report User").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            } else
                menu.add(0, REPORT_USER_ID, 0, "Report User").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case REPORT_USER_ID:
                showReportUserDialog();
                break;
            case EDIT_MY_PROFILE:
                goToEditProfile();
                break;
        }
        return true;
    }

    private void goToEditProfile() {
        Intent mIntent = new Intent(UserProfileViewActivity.this, EditProfileActivity.class);
        mIntent.putExtra("userName", ParseUser.getCurrentUser().getUsername());
        mIntent.putExtra("level", ParseUser.getCurrentUser().getString("level"));
        mIntent.putExtra("gender", ParseUser.getCurrentUser().getString("gender"));
        mIntent.putExtra("age", ParseUser.getCurrentUser().getString("age"));
        mIntent.putExtra("tagLine", ParseUser.getCurrentUser().getString("tagLine"));
        mIntent.putExtra("location", ParseUser.getCurrentUser().getString("location"));
        startActivity(mIntent);
    }

    private void showReportUserDialog() {
        reasonReport = "Spam";
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Report User")
                .titleColorRes(R.color.teal_700)
                .customView(R.layout.dialog_report, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        reportUser();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        RadioGroup radioGroupInput = (RadioGroup) dialog.getCustomView().findViewById(R.id.dialog_report_radio_group);
        radioGroupInput.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.spamRadioButton:
                        reasonReport = "Spam";
                        break;
                    case R.id.offensiveRadioButton:
                        reasonReport = "Offensive";
                        break;
                    case R.id.otherRadioButton:
                        reasonReport = "Other";
                        break;
                }
            }
        });
        EditText descriptionInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_report_desc);
        descriptionInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    descriptionReport = s.toString().trim();
                    positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.teal_700)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private void reportUser() {
        if (isUserPresent) {
            showProgressDialog("Sending Message...");
            ParseObject reportUser = new ParseObject("ReportedUsers");
            reportUser.put("userObjectId", userObject.getObjectId());
            reportUser.put("userName", userObject.getUsername());
            reportUser.put("gender", userObject.getString("gender"));
            reportUser.put("age", userObject.getString("age"));
            reportUser.put("reason", reasonReport);
            reportUser.put("description", descriptionReport);
            if (ParseUser.getCurrentUser() != null) {
                reportUser.put("senderObjectId", ParseUser.getCurrentUser().getObjectId());
                reportUser.put("senderUserName", ParseUser.getCurrentUser().getUsername());
                reportUser.put("senderGender", ParseUser.getCurrentUser().getString("gender"));
                reportUser.put("senderAge", ParseUser.getCurrentUser().getString("age"));
            }
            reportUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    progressDialog.dismiss();
                    if (e == null) {
                        Toast.makeText(UserProfileViewActivity.this, "Report Sent", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(UserProfileViewActivity.this, "Oops ! Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            Toast.makeText(UserProfileViewActivity.this, "Oops ! Something went wrong", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "Reporting null user");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUserData();
    }

    private void updateUserData() {
        userProfileViewProgressBar.setVisibility(View.VISIBLE);
        userProfileViewLayout.setVisibility(View.GONE);
        getPickupLineCount();
        fetchUserData();
    }
}
