package com.pickup.pickrup.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.pickup.pickrup.PickrUpApp;
import com.pickup.pickrup.R;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sanat Dutta on 3/5/2015.
 */
public class EmailSignUpActivity extends ActionBarActivity {

    private String TAG = "EmailSignUpActivity";

    //Interface
    MaterialDialog progressDialog;

    //Views
    private Spinner genderSpinner, ageSpinner;
    private EditText usernameEditText, emailEditText, passwordEditText, passwordAgainEditText;
    private TextView signUpButton;

    //Data
    private int MAX_USERNAME_LENGTH = 20;
    private String genderOption = "Male";
    private int ageOption = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "EmailSignUpActivity: onCreate()");
        setContentView(R.layout.activity_email_signup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        findViews();
        setUpSpinner();
        setUpSpinnerListener();
        setSignUpButtonOnClickListener();
    }

    private void setUpSpinnerListener() {
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) genderOption = "Male";
                else genderOption = "Female";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ageOption = position + 16;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setUpSpinner() {
        ArrayAdapter<CharSequence> mAdapter = ArrayAdapter.createFromResource(this,
                R.array.gender_type, android.R.layout.simple_spinner_item);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(mAdapter);

        List<String> ageList = new ArrayList<>();
        for (int i = 16; i < 100; i++) {
            ageList.add(Integer.toString(i));
        }


        ArrayAdapter<String> mAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ageList);
        mAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ageSpinner.setAdapter(mAdapter1);
    }

    private void setSignUpButtonOnClickListener() {
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String errorString = null;
                if (!passwordAgainEditText.getText().toString().equals(passwordEditText.getText().toString())) {
                    errorString = "Passwords do not match";
                }
                if (passwordEditText.getText().toString().length() < 8) {
                    errorString = "Password must be 8 characters or more";
                }
                if (passwordEditText.getText().toString().equals("")) {
                    errorString = "Password field is empty";
                }
                if (passwordEditText.getText().toString().contains(" ")) {
                    errorString = "Password cannot contain blank spaces";
                }
                if (!isEmailValid(emailEditText.getText().toString())) {
                    errorString = "Email is not valid";
                }
                if (emailEditText.getText().toString().equals("")) {
                    errorString = "Email field is empty";
                }
                if (emailEditText.getText().toString().contains(" ")) {
                    errorString = "Email cannot contain blank spaces";
                }
                if (usernameEditText.getText().toString().length() > MAX_USERNAME_LENGTH) {
                    errorString = "Username must be "+MAX_USERNAME_LENGTH+" characters or less";
                }
                if (usernameEditText.getText().toString().equals("")) {
                    errorString = "Username field is empty";
                }
                if (usernameEditText.getText().toString().contains(" ")) {
                    errorString = "Username cannot contain blank spaces";
                }

                if (errorString == null) {
                    signUpParse(usernameEditText.getText().toString(), genderOption, Integer.toString(ageOption), emailEditText.getText().toString(), passwordEditText.getText().toString());
                } else
                    Toast.makeText(EmailSignUpActivity.this, "" + errorString, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void signUpParse(final String userName, final String gender, final String age, String email, String passWord) {

        showProgressDialog("Signing Up...");

        ParseUser newUser = new ParseUser();
        newUser.setUsername(userName);
        newUser.setPassword(passWord);
        newUser.setEmail(email);
        newUser.put("gender", gender);
        newUser.put("age", age);
        newUser.put("level", "PickupNoob");

        newUser.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    setUpUserStatusClass(ParseUser.getCurrentUser().getObjectId(), userName, gender, age);
                } else {
                    if (e.getCode() == PickrUpApp.USERNAME_TAKEN)
                        Toast.makeText(EmailSignUpActivity.this, "Username Already Taken", Toast.LENGTH_SHORT).show();
                    else if (e.getCode() == PickrUpApp.EMAIL_TAKEN)
                        Toast.makeText(EmailSignUpActivity.this, "Email Already Taken", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(EmailSignUpActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setUpUserStatusClass(String userId, String userName, String gender, String age) {
        showProgressDialog("Setting up account...");

        ParseObject userStatusObject = new ParseObject("UserStatus");
        userStatusObject.put("userObjectId", userId);
        userStatusObject.put("userName", userName);
        userStatusObject.put("gender", gender);
        userStatusObject.put("age", age);
        userStatusObject.put("banned", false);
        userStatusObject.put("deactivated", false);

        userStatusObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                ParseUser.logOut();
                showInfoDialog();
            }
        });
    }

    private void showInfoDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Verify Account")
                .titleColorRes(R.color.teal_700)
                .content("Please check your email for the verification email and login again")
                .positiveText("Okay")
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Log.i(TAG, "Okay");
                        finish();
                    }
                }).build();

        dialog.show();
    }

    public boolean isEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.teal_700)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private void findViews() {
        usernameEditText = (EditText) findViewById(R.id.email_signup_userNameEditText);
        genderSpinner = (Spinner) findViewById(R.id.email_signup_genderSpinner);
        ageSpinner = (Spinner) findViewById(R.id.email_signup_ageSpinner);
        emailEditText = (EditText) findViewById(R.id.email_signup_emailEditText);
        passwordEditText = (EditText) findViewById(R.id.email_signup_passWordEditText);
        passwordAgainEditText = (EditText) findViewById(R.id.email_signup_passWordAgainEditText);
        signUpButton = (TextView) findViewById(R.id.email_signup_signUpButton);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
