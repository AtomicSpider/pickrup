package com.pickup.pickrup.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.pickup.pickrup.R;

/**
 * Created by Sanat Dutta on 3/5/2015.
 */
public class LoginActivity extends ActionBarActivity {

    private String TAG = "LoginActivity";

    //Views
    TextView loginTextView, signUpTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "LoginActivity: onCreate()");
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        findViews();
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToEmailLoginActivity = new Intent(LoginActivity.this, EmailLoginActivity.class);
                startActivity(goToEmailLoginActivity);
            }
        });
        signUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToEmailSignUpActivity = new Intent(LoginActivity.this, EmailSignUpActivity.class);
                startActivity(goToEmailSignUpActivity);
            }
        });
    }

    private void findViews() {
        loginTextView = (TextView) findViewById(R.id.loginLoginButton);
        signUpTextView = (TextView) findViewById(R.id.loginSignUpButton);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
