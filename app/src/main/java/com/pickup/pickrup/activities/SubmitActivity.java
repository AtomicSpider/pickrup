package com.pickup.pickrup.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pickup.pickrup.PickrUpApp;
import com.pickup.pickrup.R;

import java.util.List;

/**
 * Created by Sanat Dutta on 3/5/2015.
 */
public class SubmitActivity extends ActionBarActivity {

    private String TAG = "SubmitActivity";

    //Interface
    private MaterialDialog progressDialog;

    //Views
    private View positiveAction;
    private EditText pickupLineEditText;
    private TextView pickupLineSubmitCounterText;

    //Data
    private boolean isPickupLineOkay = false, TAG_GENERAL = false, TAG_CS = false, TAG_DIRTY = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "SubmitActivity: onCreate()");
        setContentView(R.layout.activity_submit);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        findViews();
        setCounterView();
    }

    private void setCounterView() {
        pickupLineEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().trim().length() < 501) {
                    if (s.toString().trim().length() > 0) isPickupLineOkay = true;
                    else isPickupLineOkay = false;
                    pickupLineSubmitCounterText.setText(Integer.toString(500 - s.toString().trim().length()));
                } else {
                    isPickupLineOkay = false;
                    pickupLineSubmitCounterText.setText(Integer.toString(500 - s.toString().trim().length()));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void findViews() {
        pickupLineEditText = (EditText) findViewById(R.id.pickupLineSubmitEditText);
        pickupLineSubmitCounterText = (TextView) findViewById(R.id.pickupLineSubmitCounterText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_bottom);
        }
        if (item.getItemId() == R.id.action_submit_submit)
            isPickUpLineValid();

        return true;
    }

    private void isPickUpLineValid() {
        if (isPickupLineOkay) {
            showPickUpLineTagDialog();
        } else
            Toast.makeText(SubmitActivity.this, "Pickupline cannot be blank or exceed 500 characters", Toast.LENGTH_SHORT).show();
    }

    private void showPickUpLineTagDialog() {
        TAG_GENERAL = true;
        TAG_CS = false;
        TAG_DIRTY = false;
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Please select one or more tags")
                .titleColorRes(R.color.teal_700)
                .items(R.array.pickupline_tags)
                .itemColorRes(R.color.teal_500)
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        checkIfTheUserIsBanned();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }
                })
                .itemsCallbackMultiChoice(new Integer[]{0}, new MaterialDialog.ListCallbackMulti() {
                    @Override
                    public void onSelection(MaterialDialog materialDialog, Integer[] integers, CharSequence[] charSequences) {
                        if (integers.length != 0) {
                            positiveAction.setEnabled(true);
                            TAG_GENERAL = false;
                            TAG_CS = false;
                            TAG_DIRTY = false;
                            for (int i = 0; i < integers.length; i++) {
                                switch (integers[i]) {
                                    case 0:
                                        TAG_GENERAL = true;
                                        break;
                                    case 1:
                                        TAG_CS = true;
                                        break;
                                    case 2:
                                        TAG_DIRTY = true;
                                        break;
                                }
                            }
                        }
                        else positiveAction.setEnabled(false);
                    }
                })
                .alwaysCallMultiChoiceCallback()
                .positiveText("Submit")
                .negativeText("Cancel")
                .build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        dialog.show();
        positiveAction.setEnabled(true);
    }

    private void checkIfTheUserIsBanned() {

        if (ParseUser.getCurrentUser() != null) {
            showProgressDialog("Submitting...");
            Log.i(TAG, "User not null, Checking for user setup class");
            ParseQuery userStatusQuery = new ParseQuery("UserStatus");
            userStatusQuery.whereEqualTo("userObjectId", ParseUser.getCurrentUser().getObjectId());
            userStatusQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    if (e == null) {
                        if (list.size() != 0) {
                            Log.i(TAG, "User status class is setup");
                            if (list.get(0).getBoolean("banned")) {
                                Log.i(TAG, "User banned, Logging Out");
                                progressDialog.dismiss();
                                Intent logoutIntent = new Intent(PickrUpApp.ACTION_LOGOUT);
                                sendBroadcast(logoutIntent);
                                finish();
                            } else {
                                Log.i(TAG, "User status is okay, Post Pickup Line");
                                submitPickUpLine(pickupLineEditText.getText().toString().trim());
                            }
                        } else {
                            submitPickUpLine(pickupLineEditText.getText().toString().trim());
                            Log.i(TAG, "User status null, Can post");
                        }
                    } else {
                        progressDialog.dismiss();
                        Log.i(TAG, "Cannot check user status");
                        Toast.makeText(SubmitActivity.this, "Something went wrong, Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else Log.i(TAG, "User null !!!");
    }

    private void submitPickUpLine(String pickUpLine) {
        ParseObject mPickupLine = new ParseObject("PickUpLines");
        mPickupLine.put("pickUpLine", pickUpLine);
        if (TAG_GENERAL) mPickupLine.put("TAG_GENERAL", TAG_GENERAL);
        if (TAG_CS) mPickupLine.put("TAG_CS", TAG_CS);
        if (TAG_DIRTY) mPickupLine.put("TAG_DIRTY", TAG_DIRTY);
        mPickupLine.put("submittedUserGender", ParseUser.getCurrentUser().getString("gender"));
        mPickupLine.put("submittedUserId", ParseUser.getCurrentUser().getObjectId());
        mPickupLine.put("submittedUserName", ParseUser.getCurrentUser().getUsername());

        mPickupLine.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    Toast.makeText(SubmitActivity.this, "New Pickup Line Submitted", Toast.LENGTH_SHORT).show();
                    finish();
                    overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_bottom);
                } else
                    Toast.makeText(SubmitActivity.this, "Error: " + e, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.teal_700)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_bottom);
    }
}
