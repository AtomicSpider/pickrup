package com.pickup.pickrup.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pickup.pickrup.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanat Dutta on 2/26/2015.
 */
public class EditProfileActivity extends ActionBarActivity {

    private String TAG = "EditProfileActivity";

    //Views
    private MaterialDialog progressDialog;
    private ImageView userProfilePictureImageView;
    private TextView userNameTextView, levelTextView, genderTextView;
    private EditText tagLineEditText, locationEditText;
    private Spinner ageSpinner;

    //Data
    private String userName, level, gender, age, tagLine, location;
    private int ageOption = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "onCreate()");

        getIntentData();
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_edit_profile);
        findViews();
        setUpSpinner();
        setUpSpinnerListener();
        chooseDefaultAge();
        displayUserDetails();
    }

    private void chooseDefaultAge() {
        int ageInt = Integer.parseInt(age);
        ageSpinner.setSelection(ageInt - 16);
    }

    private void setUpSpinnerListener() {
        ageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ageOption = position + 16;
                Log.i(TAG, "" + ageOption);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setUpSpinner() {
        List<String> ageList = new ArrayList<>();
        for (int i = 16; i < 100; i++) {
            ageList.add(Integer.toString(i));
        }
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ageList);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ageSpinner.setAdapter(mAdapter);
    }

    private void getIntentData() {
        userName = getIntent().getStringExtra("userName");
        level = getIntent().getStringExtra("level");
        gender = getIntent().getStringExtra("gender");
        age = getIntent().getStringExtra("age");
        tagLine = getIntent().getStringExtra("tagLine");
        location = getIntent().getStringExtra("location");
    }

    private void findViews() {
        userProfilePictureImageView = (ImageView) findViewById(R.id.user_profile_view_profile_picture);
        userNameTextView = (TextView) findViewById(R.id.user_profile_view_username);
        levelTextView = (TextView) findViewById(R.id.user_profile_view_level);
        genderTextView = (TextView) findViewById(R.id.user_profile_view_gender);
        ageSpinner = (Spinner) findViewById(R.id.edit_profile_ageSpinner);
        tagLineEditText = (EditText) findViewById(R.id.edit_profile_tagline);
        locationEditText = (EditText) findViewById(R.id.edit_profile_location);
    }

    private void displayUserDetails() {

        userNameTextView.setText(userName);
        levelTextView.setText(level);
        if (tagLine != null && tagLine != "")
            tagLineEditText.setText(tagLine);
        else tagLineEditText.setText("");
        if (location != null && location != "")
            locationEditText.setText(location);
        else locationEditText.setText("");
        genderTextView.setText(", " + gender);
        if (gender.equals("Female"))
            userProfilePictureImageView.setImageResource(R.drawable.ic_profile_girl_blue);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_save_profile:
                saveProfile();
                break;
        }
        return true;
    }

    private void saveProfile() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        currentUser.put("age", String.valueOf(ageOption));
        currentUser.put("tagLine", tagLineEditText.getText().toString().trim());
        currentUser.put("location", locationEditText.getText().toString().trim());
        showProgressDialog("Saving Profile...");
        currentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    finish();
                    Toast.makeText(EditProfileActivity.this, "Profile Saved", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(EditProfileActivity.this, "Could not save profile. Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.teal_700)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
