package com.pickup.pickrup.activities;// Created by Sanat Dutta on 2/12/2015.

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.pickup.pickrup.R;

public class DonateActivity extends ActionBarActivity {

    private String TAG = "DonateActivity";

    //Views
    private Spinner mSpinner;
    private TextView mDonateButton;

    //Data
    private int donateOption = 0;
    private String URL_A = "https://www.paypal.com/cgi-bin/webscr?business=sonu4414@gmail.com&cmd=_xclick&currency_code=USD&amount=";
    private String URL_B = null;
    private String URL_C = "&item_name=Donate";
    private String redirectUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_donate);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        findViews();
        setUpSpinner();
        setUpSpinnerListener();

        setDonateButtonClickListener();
    }

    private void setDonateButtonClickListener() {
        mDonateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeRedirectUrl();
                goToPaypalDonatePage();
            }
        });
    }

    private void goToPaypalDonatePage() {
        Intent goToPaypalDonatePage = new Intent(Intent.ACTION_VIEW);
        goToPaypalDonatePage.setData(Uri.parse(redirectUrl));
        startActivity(goToPaypalDonatePage);
        finish();
    }

    private void makeRedirectUrl() {
        switch (donateOption){
            case 0:
                URL_B = "1";
                break;
            case 1:
                URL_B = "2";
                break;
            case 2:
                URL_B = "3";
                break;
            case 3:
                URL_B = "5";
                break;
            case 4:
                URL_B = "10";
                break;
            case 5:
                URL_B = "20";
                break;
            case 6:
                URL_B = "30";
                break;
            case 7:
                URL_B = "50";
                break;
            case 8:
                URL_B = "100";
                break;
            case 9:
                URL_B = "200";
                break;
            case 10:
                URL_B = "300";
                break;
            case 11:
                URL_B = "500";
                break;
            default:
                URL_B = "1";
        }

        redirectUrl = URL_A+URL_B+URL_C;
    }

    private void setUpSpinnerListener() {
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                donateOption = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void findViews() {
        mSpinner = (Spinner) findViewById(R.id.donateAmountSpinner);
        mDonateButton = (TextView) findViewById(R.id.donateButton);
    }

    private void setUpSpinner() {
        ArrayAdapter<CharSequence> mAdapter = ArrayAdapter.createFromResource(this,
                R.array.donation_amount, android.R.layout.simple_spinner_item);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
