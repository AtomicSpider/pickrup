package com.pickup.pickrup;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParsePush;

/**
 * Created by Sanat Dutta on 2/24/2015.
 */
public class PickrUpApp extends Application {

    private String TAG = "PickrUpApp";

    public static String ACTION_LOGOUT = "com.pickup.pickrup.ACTION_LOGOUT";

    public static int RESET_PASSWORD_EMAIL_NOT_FOUND = 205;
    public static int INVALID_LOGIN_CREDENTIALS = 101;
    public static int USERNAME_TAKEN = 202;
    public static int EMAIL_TAKEN = 203;

    @Override
    public void onCreate() {
        super.onCreate();

        //Enable Parse crash reports
        ParseCrashReporting.enable(this);

        //initialize Parse
        Parse.initialize(this, getResources().getString(R.string.parse_app_id), getResources().getString(R.string.parse_client_id));

        ParsePush.subscribeInBackground("");
    }
}
